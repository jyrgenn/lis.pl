# Eval

package Eval;

use warnings;
no warnings 'recursion';
use strict;
use 5.014;

use Data::Dumper;

use Global;
use Object;
use Debug;
use Util;
use Print;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( Eval evalfun funcall eval_count eval_level
                      enter_errset leave_errset in_errset
                    );
}

my $eval_counter = 0;
my $errset_level = 0;

sub enter_errset {
        $errset_level++;
}

sub leave_errset {
        $errset_level--;
}

sub in_errset {
        return !!$errset_level;
}

sub eval_level {
        my ($new) = @_;
        my $l = $eval_depth;
        $eval_depth = $new if defined($new);
        return $l;
}

sub debugl {
        debug("  " x $eval_depth, @_);
}


sub eval_args {
        my ($arglist) = @_;
        my $newlist = $Nil;
        my $end;

        #debugl("eval_args on %s", $arglist);
        while (consp($arglist)) {
                my $arg;
                ($arg, $arglist) = cxr($arglist);
                my $newpair = cons(Eval($arg), $Nil);
                if ($end) {
                        rplacd($end, $newpair);
                        $end = $newpair;
                } else {
                        $end = $newlist = $newpair;
                }
        }
        #debugl("eval_args returns %s", $newlist);
        return $newlist;
}

sub call_form {
        my ($func, $args) = @_;
        my $orig_args = $args;          # for a stack trace
        my $form = function_code($func);
        my $params = car($form);
        my $body = cdr($form);
        my $inrest = 0;
        my $optional = 0;
        my $n_bindings = 0;

        my $saved_env = enter_environment(function_env($func));
        my $result = eval {
                my $result;
                # save previous bindings (possibly undef) and bind argument
                # values to parameters
                while (consp($params)) {
                        my $param;
                        ($param, $params) = cxr($params);
                        unless (symbolp($param)) {
                                error("param of function %s not a symbol: %s",
                                      function_name($func), $param);
                        }

                        if ($param == $andRest) {
                                $inrest = 1;
                                next;
                        }
                        if ($param == $andOptional) {
                                $optional = 1;
                                next;
                        }

                        if ($inrest) {
                                #debugl("bind rest (set %s %s)", $param, $args);
                                bind($param, $args);
                                $args = $Nil;
                                if (consp($params)) {
                                        error("too many params for function %s"
                                              ." after &rest: %s",
                                              function_name($func), $params);
                                }
                                last;
                        }
                        unless ($optional || consp($args)) {
                                error("too few args for function %s",
                                      function_name($func));
                        }
                        #debugl("bind param (set %s %s)", $param, car($args));
                        bind($param, car($args));
                        $args = cdr($args);
                }
                if (consp($args)) {
                        error("too many arguments for function %s",
                              function_name($func));
                }
                # finally, evaluate body forms
                while (consp($body)) {
                        my $form;
                        ($form, $body) = cxr($body);
                        $result = Eval($form);
                }
                return $result;
        };
        backto_environment($saved_env);
        # error after restoring bindings
        unless (defined($result)) {
                say("$eval_depth: ", princs(cons(function_name($func),
                                                 $orig_args)))
                    unless in_errset();
                error($@);
        }
        return $result;
}

sub funcall {
        my ($func, $args) = @_;
        #debugl("funcall %s %s", $func, $args);
        my $type = function_type($func);
        my $result;
        if ($type eq 'subr') {
                my $code = function_code($func);
                $result = &$code($args);
        } elsif ($type eq 'expr') {
                $result = call_form($func, $args);
        }
        return $result;
}

sub evalfun {
        my ($ob) = @_;
        my $max = 99;
        my $orig = $ob;

        my $result;
        while (--$max) {
                my $type = type_of($ob);
                if ($type eq "Function") {
                        #debugl("evalfun function: %s", $ob);
                        $result = $ob;
                        last;
                } elsif (is_nil($ob)) {
                        error("nil function %s", $orig);
                } elsif ($type eq "Symbol") {
                        my $symbol = $ob;
                        if (my $func = symbol_function($ob)) {
                                $ob = $func;
                                #debugl("evalfun symbol %s yields "
                                #       ."function cell: %s", $symbol, $ob);
                        } else {
                                $ob = symbol_value($ob);
                                #debugl("evalfun symbol %s yields value:",
                                #       $symbol, $ob);
                        }
                        error("undefined function %s",
                                     symbol_name($symbol))
                            unless defined($ob);
                } elsif ($type eq "Pair") {
                        my $form = $ob;
                        $ob = Eval($ob);
                        #debugl("evalfun form %s returns %s", $form, $ob);
                        error("function form yields undefined: %s", $form)
                            unless defined($ob);
                } elsif ($type eq "undef") {
                        error("undefined function %s", $orig);
                } else {
                        error("not a function: %s", $orig);
                }
        }
        error("cyclic non-function?: %s", $orig) unless $max;
        return $result;
}

sub eval_count {
        my ($new) = @_;
        my $count = $eval_counter;
        $eval_counter = $new if defined($new);
        return $count;
}

sub Eval {
        my ($ob) = @_;
        #debugl("eval expr: %s", $ob);

        $eval_depth++;
        $eval_counter++;
        my $type = type_of($ob);
        my $result = eval {
                if ($type eq "Symbol") {
                        #debugl("expr is sym: %s", $ob);
                        my $result = symbol_value($ob);
                        error("unbound variable %s", symbol_name($ob))
                            unless defined($result);
                        return $result;
                } elsif ($type eq "Pair") {
                        #debugl("expr is cons: %s", $ob);
                        my $func = evalfun(car($ob));
                        my $args = cdr($ob);
                        $args = eval_args($args) unless specialp($func);
                        my $result = funcall($func, $args);
                        error("form yields undefined value: %s", princs($ob))
                            unless defined($result);
                        return $result;
                } else {
                        return $ob;
                }
        };
        $eval_depth--;
        if ($@) {
                say("$eval_depth: ", princs($ob)) unless in_errset();
                error($@);
        }
        #debugl("eval expr: %s, returns %s", $ob, $result);
        return $result;
}

1;
