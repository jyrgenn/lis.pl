## Object representations, the low-level details of the objects, and
## create/set/get functions, also in parts for strings and numbers, although
## these are actually just Perl numbers and strings. (And as such, only
## partially distinguishable.)

package Object;

use warnings;
use strict;
use 5.014;
use Carp;

use Scalar::Util qw(looks_like_number);
use Data::Dumper;

use Global;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( intern cons car cdr listp symbolp numberp stringp
                      consp symbol_name symbol_function functionp cxr type_of
                      rplaca rplacd fset function put get symbol_plist
                      specialp cadr cddr function_type function_code
                      symbol_value set function_name is_nil all_symbols remprop
                      function_documentation is_t cons_count function_params
                      enter_environment backto_environment bind defvar
                      environmentp function_env the_environment
                      symbol_value_in_env env_vars
                    );
}

######## Variables

my %symbols = ();                       # table of all symbols
my $cons_counter = 0;                   # for eval statistics
my $root_Env;                           # the global environment
my $Env;                                # the current environment


######## Symbols
# a symbol is a hash: { name => "name", func => $function, ... }
# the property list is actually the hash itself, so you can access all
# properties [sic] of a symbol from Lisp

# return the symbol with the specified name; create it, if necessary
sub intern {
        my ($name) = @_;
        return $symbols{$name} //= bless({ name => $name }, $n_Symbol);
}

# create a variable in the root environment; set its value if it hasn't existed
# before
sub defvar {
        my ($sym, $initial, $docstring) = @_;
        $sym->{doc} = $docstring // '';
        my $name = symbol_name($sym);
        $root_Env->{$name} = $initial unless exists($root_Env->{$name});
        return $sym;
}

# bind a value to a symbol in (only) the current environment
sub bind {
        my ($sym, $value) = @_;
        my $name = symbol_name($sym);
        $Env->{$name} = $value;
}

# set the variable value of a symbol; the variable's bindingis looked up in the
# current environment including its parents; throw an error if the variable is
# not bound, except if the value is undef (need this for makunbound, even if the
# variable is not bound already)
sub set {
        my ($sym, $value, $env) = @_;

        # confess("\$sym undefined") unless defined($sym);
        my $name = symbol_name($sym);
        return $env->{$name} = $value if $env;
                
        for (my $env = $Env; $env != $Nil; $env = $env->{$n_parentenv}) {
                # warn("search env ", $env->{$n_env_name}, " for ", $name);
                if (exists($env->{$name})) {
                        # warn("set $name in ", $env->{$n_env_name});
                        return $env->{$name} = $value;
                }
        }
        error("set/setq for undefined variable %s", $sym)
            if defined($value);         # enable makunbound on undefined
                                        # variables
}

# set the function value of a symbol
sub fset {
        my ($ob, $func) = @_;
        $ob->{func} = $func;
        #print(Dumper($ob));
        
}

# set a property value of a symbol
sub put {
        my ($ob, $name, $value) = @_;
        return $ob->{symbol_name($name)} = $value;
}

# get a property value of a symbol
sub get {
        my ($ob, $name, $default) = @_;
        return $ob->{symbol_name($name)} // $default;
}

# remove a property value of a symbol
sub remprop {
        my ($ob, $name) = @_;
        return delete($ob->{symbol_name($name)});
}

# return the property list of a symbol
sub symbol_plist {
        my ($ob) = @_;
        my $result = $Nil;
        while ((my ($key, $value) = each(%{$ob}))) {
                $result = cons(intern($key), cons($value, $result));
        }
        return $result;
}

# return true iff a symbol is already created
sub symbol_exists {
        my ($name) = @_;
        return exists($symbols{$name});
}

# return the name of a symbol as a string
sub symbol_name {
        my ($ob) = @_;
        return $ob->{name};
}

# return the value bound to a symbol, or undef if it isn't bound; this searches
# through the current environment and its parents
sub symbol_value {
        my ($ob, $env, $noparents) = @_;
        $env //= $Env;
        my $name = symbol_name($ob);
        for ( ; $env != $Nil; $env = $env->{$n_parentenv}) {
                if (exists($env->{$name})) {
                        return $env->{$name};
                }
                last if $noparents;
        }
        return undef;
}

# return the function bound to a symbol
sub symbol_function {
        my ($ob) = @_;
        return $ob->{func};
}

# return an array with all symbols
sub all_symbols {
        my ($env) = @_;
        my @vals = values(%symbols);
        return @vals;
}

######## Pairs
# a pair is a hash, too: { car => ..., cdr => ... }
# there are no other fields in a hash

# return a new cons made from car and  cdr
sub cons {
        my ($car, $cdr) = @_;
        $cons_counter++;
        return bless({ car => $car, cdr => $cdr }, $n_Pair);
}

# get or set the current cons counter, for statistical porpoises
sub cons_count {
        my ($new) = @_;
        my $count = $cons_counter;
        $cons_counter = $new if defined($new);
        return $count;
}

# return the car of the object (a cons cell)
sub car {
        my ($ob) = @_;
        return $ob->{car} // $Nil;
}

# return the cdr of the object (a cons cell)
sub cdr {
        my ($ob) = @_;
        return $ob->{cdr} // $Nil;
}

# return both the car and the cdr of the object (a cons cell); this is
# redundant, but saves us one function call
sub cxr {
        my ($ob) = @_;
        return ($ob->{car}, $ob->{cdr});
}

# return the car of the cdr of the object (a cons cell)
sub cadr {
        my ($ob) = @_;
        return $ob->{cdr}->{car} // $Nil;
}

# return the cdr of the cdr of the object (a cons cell)
sub cddr {
        my ($ob) = @_;
        return $ob->{cdr}->{cdr} // $Nil;
}

# replace the car of the cons
sub rplaca {
        my ($ob, $newcar) = @_;
        return $ob->{car} = $newcar;
}

# replace the car of the cons
sub rplacd {
        my ($ob, $newcdr) = @_;
        return $ob->{cdr} = $newcdr;
}

######## Functions
# a hash, too (surprise!): { type => 'expr|subr', spec => $is_special,
#                            func => $func, doc => $doc, env => $Env }

# create a new function of a Perl function or lambda, is-special flag,
# docstring, name (a symbol, for decoration in print only), and the function
# environment (optional (and currently unused)) or the current one
sub function {
        my ($func, $is_special, $doc, $name, $env) = @_;
        my $type;
        if (ref($func) eq 'CODE') {
                $type = 'subr';
        } elsif (ref($func) eq $n_Pair) {
                $type = 'expr';
        } else {
                error('function not subr or expr: %s', $func);
        }
        $env //= $Env;
        error('function: is_special undef: %s', $func)
            unless defined($is_special);
        return bless({ func => $func, type => $type, spec => $is_special,
                       doc => $doc // '', name => $name, env => $env },
                     $n_Function);
}

# return the name (symbol) of a function, the one it was defined with (but is
# not necessarily still bound to, for decorative purposes
sub function_name {
        my ($ob) = @_;
        return $ob->{name} // $Nil;
}

# return the type of a function, expr or subr
sub function_type {
        my ($ob) = @_;
        return $ob->{type};
}

# return the code of a function (subr or lambda)
sub function_code {
        my ($ob) = @_;
        return $ob->{func};
}

# return the parameter list of a function
sub function_params {
        my ($ob) = @_;
        return function_type($ob) eq 'expr' ? car($ob->{func}) : '';
}

# return the environment of a function
sub function_env {
        my ($ob) = @_;
        return $ob->{env};
}

# return the docstring of a function
sub function_documentation {
        my ($ob) = @_;
        return $ob->{doc};
}

######## Environments
# well, yes, a hash: { *parent-environment* => $parent, var1 => ... }
# the root environment's parent is $Nil
#
# if we put the symbols themselves into the hash, they are converted to strings
# of the form "Symbol=HASH(0x7f8f89a64030)", so we cannot use keys() to iterate
# over them; instead we put the symbol names as strings into the hash, so we can
# iterate over these and intern() ourselves back to the symbol
#
# in a previous version, each environment held a level counter, a name, and a
# reference to itself, but this came at some performance cost, so out again it
# went

# return the current environment (for a Builtin)
sub the_environment {
        return $Env;
}

# create a new environment with specified parent, needed separately from
# enter_environment() to create the root environment
sub new_environment {
        my ($parent) = @_;
        return bless({ $n_parentenv => $parent }, $n_Environment);
}

# be called for a lambda or let etc., creates a new environment with the
# previous (let/let*) or explicitly specified (lambda) environment as the parent
# and sets this as the current environment; returns the previous environment to
# be saved for the benefit of backto_environment()
sub enter_environment {
        my ($parent) = @_;
        my $newenv = new_environment($parent // $Env);
        my $save_env = $Env;
        $Env = $newenv;
        return $save_env;
}

# ends the current environment and goes back to the previously saved one (end of
# let/let*/dolist/lambda context)
sub backto_environment {
        $Env = $_[0];
        # warn("back to environment ", &$f_Princs($Env));
}

# return a list of the symbols bound as variables in the current environment;
# with optional $noparents, only in the actual current environment without its
# parents
sub env_vars {
        my ($env, $noparents) = @_;
        my %vars = ();
        for ( ; $env != $Nil; $env = $env->{$n_parentenv}) {
                @vars{keys(%$env)} = 1;
                last if $noparents;
        }
        return map { intern($_) } keys(%vars);
}

######## Predicates

sub is_nil {
        my ($ob) = @_;
        # we may compare directly with $Nil only once we *know* this is not a
        # string, as that would trigger a Perl error
        
        return $T if symbolp($ob) && $ob == $Nil;
        return 0;
}

sub is_t {
        my ($ob) = @_;
        return $T if symbolp($ob) && $ob == $T;
        return 0;
}

######## Type function and predicates

# return the (our) type name for the object
sub type_of {
        my ($ob) = @_;
        return "undef" unless defined($ob);
        return ref($ob) || "scalar";
}

# return true iff the object is a list (includes nil)
sub listp {
        my ($ob) = @_;
        return consp($ob) || is_nil($ob);
}

# return true iff the object is a symbol
sub symbolp {
        my ($ob) = @_;
        return ref($ob) eq $n_Symbol;
}

# return true iff the object is an environment
sub environmentp {
        my ($ob) = @_;
        return ref($ob) eq $n_Environment;
}

# return true iff the object is a function
sub functionp {
        my ($ob) = @_;
        return ref($ob) eq $n_Function;
}

# return true iff the object is a special form
sub specialp {
        my ($ob) = @_;
        return functionp($ob) && $ob->{spec};
}

# return true iff the object is a number
sub numberp {
        my ($ob) = @_;
        return ref($ob) eq "" && looks_like_number($ob);
}

# return true iff the object is a string
sub stringp {
        my ($ob) = @_;
        return ref($ob) eq ""; #  && !looks_like_number($ob)
}

# return true iff the object is a pair
sub consp {
        my ($ob) = @_;
        return ref($ob) eq $n_Pair;
}

######## Initialization
# we do a lot of Global's initialization here, because we need intern() and
# defvar() and want to keep cycles out of the dependency graph

sub init {

        $Lambda = intern("lambda");
        $andRest = intern("&rest");
        $andOptional = intern("&optional");
        $special = intern("special");
        $builtin = intern("builtin");
        $function = intern($n_function);
        $root_environment = intern($n_root_environment);

        $t_Symbol = intern("symbol");
        $t_String = intern("string");
        $t_Number = intern("number");
        $t_Pair   = intern("pair");
        $t_Function = intern("function");

        $Nil = intern("nil");           # needed early for parent of $root_Env
        $T = intern("t");               # needed early for is_nil()

        # initialize the root environment
        $root_Env = new_environment($Nil);
        $Env = $root_Env;
        $root_Env->{$n_env_name} = 'root_Env';

        # only now we can begin to set variable values
        defvar($root_environment, $root_Env);

        defvar($Nil, $Nil);
        defvar($T, $T);
}

1;
