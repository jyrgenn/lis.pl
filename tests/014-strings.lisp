

(testcmp "split-string 1" '(split-string "/Users/ni/src/jnil/lib"
                                         "/")
         '("" "Users" "ni" "src" "jnil" "lib"))
(testcmp "split-string 2" '(split-string "  take me to the matador")
         '("take" "me" "to" "the" "matador"))
(testcmp "split-string 3" '(split-string "/Users/ni/src/jnil/lib"
                                         "/" t)
         '("Users" "ni" "src" "jnil" "lib"))

(testcmp "intern 1" '(intern "lala") "lala")
(testcmp "intern 2" '(type-of (intern "lala")) "symbol")


;; (testcmp "replace 1" '(replace-in-string "hubaluba" "b(..)" "c$1") "hucaluba")
;; (testcmp "replace 2" '(replace-in-string "hubaluba" "b(.)" "c$1") "hucaluca")

;; (testcmp "string 1" '(list (string 13)) "(\"13\")")
;; (testcmp "string 2" '(list (string "abc")) "(\"abc\")")

;; (defvar Uurl)
;; (testcmp "string-upcase 1" '(setq Uurl (string-upcase url))
;;          "HTTP://GOLANG.ORG/PKG/REGEXP/#REGEXP.FINDSTRINGSUBMATCH")
;; (testcmp "string-upcase 2" '(string-upcase url 23)
;;          "http://golang.org/pkg/rEGEXP/#REGEXP.FINDSTRINGSUBMATCH")
;; (testcmp "string-upcase 3" '(string-upcase url 23 50)
;;          "http://golang.org/pkg/rEGEXP/#REGEXP.FINDSTRINGSUBmatch")
;; (testcmp "string-upcase 4" '(string-upcase url 123 50) url)

;; (testcmp "string-downcase 1" '(string-downcase Uurl)
;;          "http://golang.org/pkg/regexp/#regexp.findstringsubmatch")
;; (testcmp "string-downcase 2" '(string-downcase Uurl 23)
;;          "HTTP://GOLANG.ORG/PKG/Regexp/#regexp.findstringsubmatch")
;; (testcmp "string-downcase 3" '(string-downcase Uurl 23 50)
;;          "HTTP://GOLANG.ORG/PKG/Regexp/#regexp.findstringsubMATCH")
;; (testcmp "string-downcase 4" '(string-downcase Uurl 123 50)
;;          Uurl)

;; (testcmp "string-capitalize 1" '(string-capitalize url)
;;          "Http://Golang.Org/Pkg/Regexp/#Regexp.Findstringsubmatch")
;; (testcmp "string-capitalize 2" '(string-capitalize url 23)
;;          "http://golang.org/pkg/regexp/#Regexp.Findstringsubmatch")
;; (testcmp "string-capitalize 3" '(string-capitalize url 23 45)
;;          "http://golang.org/pkg/regexp/#Regexp.FindstringSubmatch")
;; (testcmp "string-capitalize 4" '(string-capitalize url 123 50) url)

;; (defvar wandersmann "dA gehT deR kleinE wandersManN")
;; (testcmp "string-capitalize 5" '(string-capitalize wandersmann)
;;          "Da Geht Der Kleine Wandersmann")
;; (testcmp "string-capitalize 6" '(string-capitalize wandersmann 10)
;;          "dA gehT der Kleine Wandersmann")
;; (testcmp "string-capitalize 7" '(string-capitalize wandersmann 10 27)
;;          "dA gehT der Kleine WandersmanN")

