(testcmp "quote" '(quote lala) "lala")
(testcmp "while 0" '(errset (while)) "nil")
(testcmp "while nil" '(while nil) "nil")
(testcmp "while" '(let (a
                        (b '(2 3 5 7 11 13 17 19 23 29)))
                    (while b
                      (setq a (cons (car b) a))
                      (setq b (cdr b)))
                    (length a))
         "10")
(testcmp "while list" '(let ((l '(3 4 5 6)) (sum 0))
                        (while l
                          (setq sum (+ sum (car l)))
                          (setq l (cdr l)))
                          sum) 18)
