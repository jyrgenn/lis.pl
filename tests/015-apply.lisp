(testcmp "apply to empty list" '(apply #'+ ()) 0)

(testcmp "apply to 1 list" '(apply #'+ '(3)) 3)

(testcmp "apply to short list" '(apply #'+ '(3 5)) 8)

(testcmp "apply to arg + list" '(apply #'+ 4 '(3 5)) 12)

(testcmp "apply to 2 args + list" '(apply #'+ 2 4 '(3 5)) 14)
