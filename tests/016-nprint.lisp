; test if integers are printed as integers

(defun pow (base power)
  (if (zerop power)
      1
    (* base (pow base (1- power)))))

(testcmp "nprint 1" '(pow 2 32) "4294967296")
(testcmp "nprint 2" '(format nil "%d" (1+ (pow 2 32))) "4294967297")
