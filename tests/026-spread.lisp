
;; spreadable arguments lists as with apply and list*

(testcmp "spread 0" '(list*) nil)
(testcmp "spread 1" '(list* 5) 5)
(testcmp "spread 2" '(list* 3 4 5) '(3 4 . 5))
(testcmp "spread 3" '(list* 3 4 '(5 6)) '(3 4 5 6))
(testcmp "spread 3" '(list* '(5 6)) '(5 6))
(testcmp "spread 4" '(list* nil) nil)



