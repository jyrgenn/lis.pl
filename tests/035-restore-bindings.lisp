;; restore binding after let* malformed binding error
(defvar a 315)
(testcmp "restore let* 1" '(errset (let* ((a 23)
                                          (b 112)
                                          119)
                                     "throws error due to malformed binding"))
         nil)
(testcmp "really restored? 1" 'a 315)

;; restore binding after let* undefined value error
(setq a 316)
(makunbound 'c)
(testcmp "restore let* 2" '(errset (let* ((a 24)
                                          (b c)
                                          (d 119))
                                     "throws error due to undef c"))
         nil)
(testcmp "really restored? 2" 'a 316)


;; restore binding after let malformed binding error
(setq a 317)
(testcmp "restore let 1" '(errset (let ((a 23)
                                        (b 'd)
                                        119)
                                    "throws error due to malformed binding"))
         nil)
(testcmp "really restored? 3" 'a 317)


;; restore binding after let undefined value error
(setq a 318)
(makunbound 'c)
(testcmp "restore let 2" '(errset (let ((a 24)
                                        (b c)
                                        (d 119))
                                     "throws error due to undef c"))
         nil)
(testcmp "really restored? 4" 'a 318)

;;===========

;; restore binding after regular let*
(setq a 319)
(testcmp "restore let* 3" '(let* ((a 23)
                                  (b 2)
                                  (d 119))
                             64927)
         64927)
(testcmp "really restored? 5" 'a 319)

;; restore binding after regular let
(setq a 320)
(testcmp "restore let 3" '(let ((a 23)
                                (b 2)
                                (d 119))
                            64928)
         64928)
(testcmp "really restored? 6" 'a 320)
