;#!/usr/bin/env lingo

(defvar testdir "tests")
(defvar out t)
(defvar fails nil)
(defvar ntests 0)
(defvar *current-test-file* nil "the test file currently running")

(defun testcmp (name form value)
  "Run the test called NAME and print the result.
The test is successful if the printed representations of
the evaluation of FORM and VALUE are equal."
  (let ((result (errset (if (functionp form)
                            (form)
                          (eval form))))
        (target (princs value)))
    (setq ntests (1+ ntests))
    (if (atom result)
        (progn (format out "Test FAIL: %s RAISED ERROR: %s\n"
                       name *last-error*)
               (setq fails (cons (cons name *current-test-file*) fails)))
      (let ((resultvalue (princs (car result))))
        (if (eq resultvalue target)
            (format out "Test pass: %s\t%s\n" name resultvalue)
          (format out "Test FAIL: %s\n calculated: %s\n   expected: %s\n"
                  name resultvalue target)
          (setq fails (cons (cons name *current-test-file*) fails)))))))

(testcmp 'testcmp ''lala "lala")        ;check if the *testing* works

(let ((files (or *ARGS* (glob-filenames (string-concat testdir
                                                       "/[0-9]*.lisp")))))
  (format t "load files: %s\n" (princs files))
  (while files
    (setq *current-test-file* (car files))
    (setq files (cdr files))
    (format out "\nloading %s\n" *current-test-file*)
    (load *current-test-file*)))

(format t "%d tests, %d FAILS" ntests (length fails))
(let ((rfails (reverse fails)))
  (while rfails
    (let ((err (car rfails)))
      (format t "\n  FAIL \"%s:%s\"" (cdr err) (car err))
      (setq rfails (cdr rfails)))))
(terpri)
(when fails
  (exit 1))


