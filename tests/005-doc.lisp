

(defun dog (collar &optional leash) "Waff!" (list 'dog collar leash))
(testcmp "docstring 1" '(doc #'dog nil t)
         "function: dog (collar &optional leash)\nWaff!")
(testcmp "docstring 2" '(doc 'dog t t)
         "function: dog (collar &optional leash)")
(defun cat (&optional collar) t)
(testcmp "docstring 3" '(doc 'cat nil t)
         "function: cat (&optional collar)")
(testcmp "docstring 4" '(doc 'cat t t)
         "function: cat (&optional collar)")
(testcmp "docstring 5" '(doc 'symbolp t t)
         "builtin function: symbolp")
(testcmp "docstring 6" '(doc 'symbolp nil t)
         "builtin function: symbolp
return t if OBJECT is a symbol, otherwise nil")
(testcmp "docstring 7" '(doc 'if t t)
         "builtin special form: if")
