(testcmp "substr 0" '(sublist '(m a d r i g a l) 0) '(m a d r i g a l))
(testcmp "sublist 2" '(sublist '(m a d r i g a l) 2) '(d r i g a l))
(testcmp "sublist 10" '(sublist '(m a d r i g a l) 10) ())

(testcmp "sublist 0 0" '(sublist '(m a d r i g a l) 0 0) ())
(testcmp "sublist 2 0" '(sublist '(m a d r i g a l) 2 0) ())
(testcmp "sublist 10 0" '(sublist '(m a d r i g a l) 10 0) ())

(testcmp "sublist 0 1" '(sublist '(m a d r i g a l) 0 1) '(m))
(testcmp "sublist 2 1" '(sublist '(m a d r i g a l) 2 1) ())
(testcmp "sublist 10 1" '(sublist '(m a d r i g a l) 10 1) ())

(testcmp "sublist 0 2" '(sublist '(m a d r i g a l) 0 2) '(m a))
(testcmp "sublist 2 2" '(sublist '(m a d r i g a l) 2 2) ())
(testcmp "sublist 10 2" '(sublist '(m a d r i g a l) 10 2) ())

(testcmp "sublist 0 5" '(sublist '(m a d r i g a l) 0 5) '(m a d r i))
(testcmp "sublist 2 5" '(sublist '(m a d r i g a l) 2 5) '(d r i))
(testcmp "sublist 10 5" '(sublist '(m a d r i g a l) 10 5) ())

(testcmp "sublist 0 15" '(sublist '(m a d r i g a l) 0 15) '(m a d r i g a l))
(testcmp "sublist 2 15" '(sublist '(m a d r i g a l) 2 15) '(d r i g a l))
(testcmp "sublist 10 15" '(sublist '(m a d r i g a l) 10 15) ())


(testcmp "substr 0" '(substr "madrigal" 0) "madrigal")
(testcmp "substr 2" '(substr "madrigal" 2) "drigal")
(testcmp "substr 10" '(substr "madrigal" 10) "")

(testcmp "substr 0 0" '(substr "madrigal" 0 0) "")
(testcmp "substr 2 0" '(substr "madrigal" 2 0) "")
(testcmp "substr 10 0" '(substr "madrigal" 10 0) "")

(testcmp "substr 0 1" '(substr "madrigal" 0 1) "m")
(testcmp "substr 2 1" '(substr "madrigal" 2 1) "")
(testcmp "substr 10 1" '(substr "madrigal" 10 1) "")

(testcmp "substr 0 2" '(substr "madrigal" 0 2) "ma")
(testcmp "substr 2 2" '(substr "madrigal" 2 2) "")
(testcmp "substr 10 2" '(substr "madrigal" 10 2) "")

(testcmp "substr 0 5" '(substr "madrigal" 0 5) "madri")
(testcmp "substr 2 5" '(substr "madrigal" 2 5) "dri")
(testcmp "substr 10 5" '(substr "madrigal" 10 5) "")

(testcmp "substr 0 15" '(substr "madrigal" 0 15) "madrigal")
(testcmp "substr 2 15" '(substr "madrigal" 2 15) "drigal")
(testcmp "substr 10 15" '(substr "madrigal" 10 15) "")

