
(testcmp "string of nothing" '(string) "")

(testcmp "string single list" '(string '(a b c)) "(a b c)")

(testcmp "string symbols" '(string 'abc 'def) "abcdef")

(testcmp "string strings" '(string "a" "b") "ab")

(testcmp "string mixed" '(string "a" t nil 'huhu (cons 'd '(e f)) 112)
         "atnilhuhu(d e f)112")
