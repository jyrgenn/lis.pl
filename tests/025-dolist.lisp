
(testcmp "dolist 0"
         '(let (result)
           (dolist (el '(a b c d e f g h i) (nreverse result))
             (push el result)))
         '(a b c d e f g h i))
(testcmp "dolist 1"
         '(let (result)
           (dolist (el '(a b c d e f g h i) (nreverse result) 3)
             (push el result)))
         '(d e f g h i))
(testcmp "dolist 2"
         '(let (result)
           (dolist (el '(a b c d e f g h i) (nreverse result) 3 6)
             (push el result)))
         '(d e f))

;; (testcmp "doseq 0"
;;          '(let (result)
;;            (doseq (el "abcdefghi" (apply #'string (nreverse result)))
;;              (push el result)))
;;          "abcdefghi")
;; (testcmp "doseq 1"
;;          '(let (result)
;;            (doseq (el "abcdefghi" (apply #'string (nreverse result)) 3)
;;              (push el result)))
;;          "defghi")
;; (testcmp "doseq 2"
;;          '(let (result)
;;            (doseq (el "abcdefghi" (apply #'string (nreverse result)) 3 6)
;;              (push el result)))
;;          "def")
