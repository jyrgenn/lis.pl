(testcmp "progn 0" '(progn) nil)
(testcmp "progn 1" '(progn 3) 3)
(testcmp "progn 1a" '(progn (car (cdr '(z x y)))) "x")
(testcmp "progn 2" '(progn (car (cdr '(z x y)))
                          19) "19")
(testcmp "progn 3" '(progn (car (cdr '(z x y)))
                          19
                          (cons (cons 17 4) nil)) "((17 . 4))")
