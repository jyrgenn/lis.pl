(testcmp "scope" '(let ((a 'lexical))
                    (let ((f (lambda () (print a))))
                      (let ((a 'dynamic))
                        (funcall f))))
         'lexical)

(defun make-counter (&optional init)
  (let ((counter (or init 0)))
    (lambda ()
      (prog1
          counter
        (setq counter (1+ counter))))))

(setq a (make-counter))
(setq b (make-counter 119))

(testcmp "counter a 0" '(a) 0)
(testcmp "counter a 1" '(a) 1)
(testcmp "counter a 2" '(a) 2)
(testcmp "counter b 0" '(b) 119)
(testcmp "counter b 1" '(b) 120)
(testcmp "counter a 3" '(a) 3)
(testcmp "counter b 2" '(b) 121)

