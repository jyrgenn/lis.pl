(testcmp "read simple" '(cons (read "tangeldadder") nil) "(tangeldadder)")
(testcmp "read defun" '(progn (eval (read "(defun q (n) (* n n))"))
                              (q 13)) "169")
(testcmp "read error 1" '(or (errset (progn
                                       (eval (read "(defun q (n) (* n n)"))
                                       (q 13)))
                             (cadr (split-string *last-error* ": ")))
         "EOF reading list elements")

(testcmp "read error 2" '(or (errset (progn
                                       (eval (read ")(defun q (n) (* n n))"))
                                       (q 13)))
                             (cadr (split-string *last-error* ": ")))
         "close paren unexpected")
