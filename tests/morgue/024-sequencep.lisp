
(testcmp "seqencep 0" '(sequencep nil) t)
(testcmp "seqencep 1" '(sequencep 'a) nil)
(testcmp "seqencep 2" '(sequencep "lala") t)
(testcmp "seqencep 4" '(sequencep (list 3 4 5)) t)
(testcmp "seqencep 6" '(sequencep 3.4) t) ; numbers are strings!
(testcmp "seqencep 10" '(sequencep (lambda () 3)) nil)
