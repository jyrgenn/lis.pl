(testcmp "sort string 0" '(sort "" #'string>) "")
(testcmp "sort string 1" '(sort "W" #'string>) "W")
(testcmp "sort string 2" '(sort "TF"  #'string>) "TF")
(testcmp "sort string 3" '(sort "FT"  #'string>) "TF")
(testcmp "sort string 4" '(sort "The quick brown fox jumps over the lazy dog."
                           #'string>)
         "zyxwvuutsrrqpoooonmlkjihhgfeeedcbaT.        ")
