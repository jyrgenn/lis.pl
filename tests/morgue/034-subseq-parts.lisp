(testcmp "subseq 0" '(subseq "madrigal" 0) "madrigal")
(testcmp "subseq 2" '(subseq "madrigal" 2) "drigal")
(testcmp "subseq 10" '(subseq "madrigal" 10) "")

(testcmp "subseq 0 0" '(subseq "madrigal" 0 0) "")
(testcmp "subseq 2 0" '(subseq "madrigal" 2 0) "")
(testcmp "subseq 10 0" '(subseq "madrigal" 10 0) "")

(testcmp "subseq 0 1" '(subseq "madrigal" 0 1) "m")
(testcmp "subseq 2 1" '(subseq "madrigal" 2 1) "")
(testcmp "subseq 10 1" '(subseq "madrigal" 10 1) "")

(testcmp "subseq 0 2" '(subseq "madrigal" 0 2) "ma")
(testcmp "subseq 2 2" '(subseq "madrigal" 2 2) "")
(testcmp "subseq 10 2" '(subseq "madrigal" 10 2) "")

(testcmp "subseq 0 5" '(subseq "madrigal" 0 5) "madri")
(testcmp "subseq 2 5" '(subseq "madrigal" 2 5) "dri")
(testcmp "subseq 10 5" '(subseq "madrigal" 10 5) "")

(testcmp "subseq 0 15" '(subseq "madrigal" 0 15) "madrigal")
(testcmp "subseq 2 15" '(subseq "madrigal" 2 15) "drigal")
(testcmp "subseq 10 15" '(subseq "madrigal" 10 15) "")

