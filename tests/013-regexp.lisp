(defvar url "http://golang.org/pkg/regexp/#Regexp.FindStringSubmatch")
(defvar urlre "^(([^:/?\#]+)://)?([^/?\#:]*)(:([0-9]+))?(/.*)?")
(testcmp "regexp 1" '(re-match "(bc+)" "abcdef") "(\"bc\")")
(testcmp "regexp 2" '(re-match "^(http://)([a-zA-Z0-9\.-]+)(:([0-9]+))?/?$"
                      url) nil)
(testcmp "regexp 3" '(re-match urlre url)
         "(\"http://\" \"http\" \"golang.org\" nil nil \"/pkg/regexp/#Regexp.FindStringSubmatch\")")

