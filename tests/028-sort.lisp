
(testcmp "sort list 0" '(sort '() #'>) '())
(testcmp "sort list 1" '(sort '(60) #'>) '(60))
(testcmp "sort list 2" '(sort '(60 94) #'>) '(94 60))
(testcmp "sort list 3" '(sort '(94 60) #'>) '(94 60))
(testcmp "sort list 4" '(sort '(94 60 23 24) #'>) '(94 60 24 23))
(testcmp "sort list 5" '(sort '(94 94 12 94) #'>) '(94 94 94 12))
(testcmp "sort list 6" '(sort '(94 12 94) #'>) '(94 94 12))
(testcmp "sort list 9" '(sort '(66 43 42 68 6 15 9 30 51 81
                                21 38 31 46 28 29 67 21 20 36)  #'>)
         '(81 68 67 66 51 46 43 42 38 36 31 30 29 28 21 21 20 15 9 6))
