
(defvar the-alist '((3 . 4)
                    (7 . 5)
                    (lala . humdi)
                    (10 . 11)
                    ((1 2 3) . 12)
                    ("hudi" . :rudi)))

(testcmp "assoc 0" '(assoc 'lala nil) nil)
(testcmp "assoc 1" '(errset (assoc 'lala '(4))) nil)
(testcmp "assoc 2" '(assoc 'lala the-alist) '(lala . humdi))
(testcmp "assoc 3" '(assoc '(1 2 3) the-alist) '((1 2 3) . 12))
(testcmp "assoc 4" '(assoc '(1 2 5) the-alist) nil)
(testcmp "assoc 5" '(assoc 10 the-alist) '(10 . 11))

(testcmp "assq 0" '(assq 'lala nil) nil)
(testcmp "assq 1" '(errset (assq 'lala '(4))) nil)
(testcmp "assq 2" '(assq 'lala the-alist) '(lala . humdi))
(testcmp "assq 3" '(assq '(1 2 3) the-alist) nil)
(testcmp "assq 4" '(assq '(1 2 5) the-alist) nil)

(defun default () 'this)
(defvar the-function 'default)

(testcmp "sassoc 0" '(sassoc 'lala nil #'default) 'this)
(testcmp "sassoc 1" '(errset (sassoc 'lala '(4) #'default)) nil)
(testcmp "sassoc 2" '(sassoc 'lala the-alist #'default) '(lala . humdi))
(testcmp "sassoc 3" '(sassoc '(1 2 3) the-alist #'default) '((1 2 3) . 12))
(testcmp "sassoc 4" '(sassoc '(1 2 5) the-alist #'default) 'this)
(testcmp "sassoc 5" '(sassoc 10 the-alist 'default) '(10 . 11))

(testcmp "sassq 0" '(sassq 'lala nil #'default) 'this)
(testcmp "sassq 1" '(errset (sassq 'lala '(4) #'default)) nil)
(testcmp "sassq 2" '(sassq 'lala the-alist #'default) '(lala . humdi))
(testcmp "sassq 3" '(sassq '(1 2 3) the-alist #'default) 'this)
(testcmp "sassq 4" '(sassq '(1 2 5) the-alist the-function) 'this)

