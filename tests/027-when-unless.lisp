
(defun false ()
  nil)

(defun true ()
  t)

(testcmp "when 0" '(when (false) (+ 13 14) (* 15 16) 332) nil)
(testcmp "when 1" '(when (true) (+ 13 14) (* 15 16) 332) 332)
(testcmp "when 2" '(let (a) (when (true) (setq a 115) 19) a) 115)
(testcmp "unless 0" '(unless (false) (+ 13 14) (* 15 16) 332) 332)
(testcmp "unless 1" '(unless (true) (+ 13 14) (* 15 16) 332) nil)
(testcmp "unless 2" '(let (a) (unless (false) (setq a 115) 19) a) 115)
