
(testcmp "delete list 0" '(delete 'a '(b)) '(b))
(testcmp "delete list 1" '(delete 'a '(a)) nil)
(testcmp "delete list 2" '(delete 'c '(a b c d e f)) '(a b d e f))
(testcmp "delete list 3" '(delete 'a '(a b c d e f)) '(b c d e f))
(testcmp "delete list 4" '(delete 'f '(a b c d e f)) '(a b c d e))
(testcmp "delete list 5" '(delete 'g '(a b c d e f)) '(a b c d e f))
(testcmp "delete list 6" '(delete 'g nil) nil)
(testcmp "delete list 7" '(errset (delete 'g '(a b c d e . f))) nil)
(testcmp "delete list 8" '(delete 'g '(a g b g c g)) '(a b c))

(testcmp "delete symbol 0" '(delete 'a nil) nil)
(testcmp "delete symbol 1" '(errset (delete 'a 'b)) nil)
