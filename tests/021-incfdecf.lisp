
(let ((n 4)
      (m 7.25)
      (o -123456))
  (testcmp "incf 1" (lambda () (incf n) n) "5")
  (testcmp "incf 2" (lambda () (incf n)) "6")
  (testcmp "incf 3" (lambda () (incf n 3)) "9")
  (testcmp "incf 4" (lambda ()
                      (errset (incf n 'b))
                      *last-error*)
           "incf: delta arg is not numeric: b")
  (testcmp "incf 5" (lambda () (incf m)) "8.25")
  (testcmp "incf 6" (lambda () (incf o (- 3))) "-123459")
  (testcmp "decf 1" (lambda ()
                      (decf n)
                      n)
           "8")
  (testcmp "decf 2" (lambda () (decf n)) "7")
  (testcmp "decf 3" (lambda () (decf n 3)) "4")
  (testcmp "decf 4" (lambda()
                      (errset (decf n 'b))
                      *last-error*)
           "decf: delta arg is not numeric: b")
  (testcmp "decf 5" (lambda () (decf m)) "7.25")
  (testcmp "decf 6" (lambda () (decf o (- 3))) "-123456")
  )
