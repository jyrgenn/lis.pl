
(defvar tree1 '(a . b))
(testcmp "car" '(car tree1) "a")
(testcmp "cdr" '(cdr tree1) "b")

(defvar tree2 '((a . b) .
              (c . d)))
(testcmp "caar" '(caar tree2) "a")
(testcmp "cdar" '(cdar tree2) "b")
(testcmp "cadr" '(cadr tree2) "c")
(testcmp "cddr" '(cddr tree2) "d")

(defvar tree3 '(((a . b) .
               (c . d)) .
              ((e . f) .
               (g . h))))
(testcmp "caaar" '(caaar tree3) "a")
(testcmp "cdaar" '(cdaar tree3) "b")
(testcmp "cadar" '(cadar tree3) "c")
(testcmp "cddar" '(cddar tree3) "d")
(testcmp "caadr" '(caadr tree3) "e")
(testcmp "cdadr" '(cdadr tree3) "f")
(testcmp "caddr" '(caddr tree3) "g")
(testcmp "cdddr" '(cdddr tree3) "h")

(defvar tree4
  (let ((*next-tag* 0))
    (labels ((next-symbol ()
                          (prog1
                              (intern (format nil "%04b" *next-tag*))
                            (setq *next-tag* (1+ *next-tag*))))
             (mktree (depth)
                     (if (zerop depth)
                         (next-symbol)
                       (cons (mktree (1- depth))
                             (mktree (1- depth))))))
      (mktree 4))))
(format t "%s\n" tree4)
(testcmp "caaaar" '(caaaar tree4) "0000")
(testcmp "cdaaar" '(cdaaar tree4) "0001")
(testcmp "cadaar" '(cadaar tree4) "0010")
(testcmp "cddaar" '(cddaar tree4) "0011")
(testcmp "caadar" '(caadar tree4) "0100")
(testcmp "cdadar" '(cdadar tree4) "0101")
(testcmp "caddar" '(caddar tree4) "0110")
(testcmp "cdddar" '(cdddar tree4) "0111")
(testcmp "caaadr" '(caaadr tree4) "1000")
(testcmp "cdaadr" '(cdaadr tree4) "1001")
(testcmp "cadadr" '(cadadr tree4) "1010")
(testcmp "cddadr" '(cddadr tree4) "1011")
(testcmp "caaddr" '(caaddr tree4) "1100")
(testcmp "cdaddr" '(cdaddr tree4) "1101")
(testcmp "cadddr" '(cadddr tree4) "1110")
(testcmp "cddddr" '(cddddr tree4) "1111")
