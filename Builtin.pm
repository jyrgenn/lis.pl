# Builtin functions

package Builtin;

use warnings;
no warnings 'recursion';
use strict;
use 5.014;

use Data::Dumper;

use Global;
use Object;
use Print;
use Debug;
use Util;
use Eval;
use Interp;

# Builtins get their arguments directly as a Lisp list, have names
# beginning with 'B', and are defined here (except for, well, exceptions)

sub Bthe_environment {
        checkargs($_[0], '');
        return the_environment();
}

sub Benvironment {
        my ($env, $noparents) = checkargs($_[0], ':Ee');
        my %vars = ();
        $env = the_environment() if is_nil($env);
        while (defined($env)) {
                while (my ($key, $value) = each(%$env)) {
                        $vars{$key} //= $value;
                }
                last if $noparents;
                $env = $env->{$n_parentenv};
        }
        my $result = $Nil;
        while (my ($key, $value) = each(%vars)) {
                $result = cons(cons(intern($key), $value), $result);
        }
        return $result;
}

sub Bwhen {
        my ($cond, $bodyforms) = checkargs($_[0], 'er');
        my $result = $Nil;
        unless (is_nil(Eval($cond))) {
                $result = eval_forms($bodyforms);
        }
        return $result;
}

sub Bunless {
        my ($cond, $bodyforms) = checkargs($_[0], 'er');
        my $result = $Nil;
        if (is_nil(Eval($cond))) {
                $result = eval_forms($bodyforms);
        }
        return $result;
}

sub Bpush {
        my ($item, $var) = checkargs($_[0], 'ey');
        my $list = Eval($var);
        my $newel = Eval($item);
        return set($var, cons($newel, $list));
}

sub Bpop {
        my ($var) = checkargs($_[0], 'y');
        my $list = Eval($var);
        error("pop: value of %s is not a list: %s", $var, $list)
            unless listp($list);
        my $item;
        ($item, $list) = cxr($list);
        set($var, $list);
        return $item;
}

sub Bsubstr {
        my ($str, $start, $end) = checkargs($_[0], 'sn:n');
        $start = 0 if $start < 0;
        my $len = length($str);
        $start = $len if $start > $len;
        if (is_nil($end)) {
                return substr($str, $start);
        } else {
                $end = $start if $end < $start;
                $end = $len if $end > $len;
                return substr($str, $start, $end - $start);
        }
}

sub Bsublist {
        my ($list, $start, $end) = checkargs($_[0], 'ln:n');
        return sublist($list, $start, $end);
}

sub sublist {
        my ($list, $start, $end) = @_;
        my $skip = $start;
        while (consp($list) && $skip > 0) {
                $list = cdr($list);
                $skip--;
        }
        return $list if is_nil($end);

        my $result = $Nil;
        my $lastpair;
        $end -= $start;
        while (consp($list) && $end > 0) {
                my $elem;
                ($elem, $list) = cxr($list);
                my $newpair = cons($elem, $Nil);
                if (defined($lastpair)) {
                        rplacd($lastpair, $newpair);
                        $lastpair = $newpair;
                } else {
                        $result = $lastpair = $newpair;
                }
                $end--;
        }
        return $result;
}

sub eval_forms {
        my ($forms) = @_;
        my $result = $Nil;
        while (consp($forms)) {
                my $form;
                ($form, $forms) = cxr($forms);
                $result = Eval($form);
        }
        return $result;
}

sub Bdolist {
        my ($formargs, $body) = checkargs($_[0], 'lr');
        my @formargs = list2array($formargs);
        #warn("formargs: @formargs");
        my $loopvar = $formargs[0];
        error("dolist: loopvar not a symbol: %s", $loopvar)
            unless symbolp($loopvar);
        my $listform = $formargs[1] or error("dolist: listform undefined");
        my $resultform = $formargs[2] // $Nil;
        my $start = $formargs[3] // 0;
        error("dolist: start arg not numeric: %s", $start)
            unless numberp($start);
        my $end = $formargs[4] // $Nil;
        error("dolist: end arg not numeric: %s", $end)
            unless is_nil($end) || numberp($end);

        my $saved_env = enter_environment();
        my $result = eval {
                my $list = sublist(Eval($listform), $start, $end);
                while (consp($list)) {
                        my $elem;
                        ($elem, $list) = cxr($list);
                        bind($loopvar, $elem);
                        eval_forms($body);
                }
                return Eval($resultform);
        };
        backto_environment($saved_env);
        error($@) if $@;
        return $result;
}

sub Bincf {
        my ($var, $delta) = checkargs($_[0], 'y:e');
        my $num = Eval($var);
        error("incf: %s has no numeric value: %s", $var, $num)
            unless numberp($num);
        if (is_nil($delta)) {
                $delta = 1;
        } else {
                $delta = Eval($delta);
                error("incf: delta arg is not numeric: %s", $delta)
                    unless numberp($delta);
        }
        return set($var, $num + $delta);
}

sub Bdecf {
        my ($var, $delta) = checkargs($_[0], 'y:e');
        my $num = Eval($var);
        error("decf: %s has no numeric value: %s", $var, $num)
            unless numberp($num);
        if (is_nil($delta)) {
                $delta = 1;
        } else {
                $delta = Eval($delta);
                error("decf: delta arg is not numeric: %s", $delta)
                    unless numberp($delta);
        }
        return set($var, $num - $delta);
}

sub Bcond {
        my ($arglist) = @_;
        while (consp($arglist)) {
                my $clause;
                ($clause, $arglist) = cxr($arglist);
                my ($condition, $clausebody) = cxr($clause);
                if (!is_nil(Eval($condition))) {
                        return eval_forms($clausebody);
                }
        }
        return $Nil;
}

sub Bfunction_environment {
        my ($func) = checkargs($_[0], 'e');
        $func = evalfun($func);
        return function_env($func);
}

sub Benv_vars {
        my ($env, $noparent) = checkargs($_[0], ':Ee');
        $env = the_environment() if is_nil($env);
        return array2list(env_vars($env, !is_nil($noparent)));
}

sub Benv_ref {
        my ($env, $symbol, $noparents) = checkargs($_[0], 'Ey:e');
        return symbol_value($symbol, $env, !is_nil($noparents));
}

sub Bdefvar {
        my ($symbol, $initial, $docstring) = checkargs($_[0], 'y:es');
        return defvar($symbol, Eval($initial), Eval($docstring));
}

sub Btruncate {
        my ($number) = checkargs($_[0], 'n');
        return int($number);
}

sub Bshell {
        my ($command, $return_output) = checkargs($_[0], 'S:e');
        my $result;
        if (is_nil($return_output)) {
                $result = tornil(!system($command));
        } else {                
                $result = `$command`;
        }
        return $result;
}

sub Bread {
        my ($input) = checkargs($_[0], ':S');
        if (is_nil($input)) {
                return Read::Read();
        } elsif (stringp ($input)) {
                return Read::Read($input);
        } else {
                ...
        }
}

sub Brandom {
        my ($limit) = checkargs($_[0], ':n');
        $limit = 1 if is_nil($limit);
        return rand($limit);
}

sub Bsqrt {
        my ($arg) = checkargs($_[0], 'n');
        return sqrt($arg);
}

sub Bsplit_string {
        my ($string, $sep, $nonulls) = checkargs($_[0], 'S:ee');
        my $re;
        if (is_nil($sep)) {
                $re = ' ';              # special case: white space with nonull
                                        # read; see perlfunc manual on split
                $string =~ s/^\s+//;    # this one's needed for a Perl 5.014 bug
        } else {
                my $re_string = princs($sep);
                $re = qr/$re_string/;
        }
        my @result = split($re, $string);
        @result = grep(/./, @result) unless is_nil($nonulls);
        return array2list(@result);
}

sub Bsymbol_plist {
        my ($sym) = checkargs($_[0], 'y');
        return symbol_plist($sym);
}

sub Bget {
        my ($sym, $ind, $default) = checkargs($_[0], 'yy:e');
        return get($sym, $ind, $default);
}

sub Bput {
        my ($sym, $ind, $val) = checkargs($_[0], 'yye');
        return put($sym, $ind, $val);
}

sub Bremprop {
        my ($sym, $ind) = checkargs($_[0], 'yy');
        return tornil(remprop($sym, $ind));
}

# return list of OBJECT's type, name, value, function, and property list
sub Bdescribe {
        my ($ob) = checkargs($_[0], 'e');
        my $type = Btype_of(cons($ob, $Nil));
        if ($type == $t_Symbol) {
                my $value = symbol_value($ob);
                $value = cons($value, $Nil) if defined($value);
                return array2list($type, $ob, $value, symbol_plist($ob));
        } elsif ($type == $t_Function) {
                return array2list(specialp($ob) ? $special : $function,
                                  consp($ob->{func}) ? $Lambda : $builtin,
                                  function_documentation($ob), $ob);
        } else {
                return array2list($type, $ob);
        }
}

sub Bglob_filenames {
        my ($pattern) = checkargs($_[0], 'S');
        my @entries = glob($pattern);
        return array2list(@entries);
}

sub Bexit {
        my ($statusarg) = checkargs($_[0], 'e');
        my $status;
        if (is_t($statusarg)) {
                $status = 0;
        } elsif (is_nil($statusarg)) {
                $status = 1;
        } elsif (numberp($statusarg)) {
                $status = $statusarg;
        } else {
                $status = 0;
        }
        exit($status);
}

sub Bformat {
        my ($direction, $format, @args) = checkargs($_[0], 'eSR');
        @args = map {princs($_)} @args;
        if (is_nil($direction)) {
                return sprintf($format, @args);
        } elsif (is_t($direction)) {
                printf($format, @args);
                return $Nil;
        } else {
                ...
        }
}

sub Berrset {
        my ($expr) = checkargs($_[0], 'e');
        enter_errset();
        my $result = eval { Eval($expr) };
        leave_errset();
        if (defined($result)) {
                return cons($result, $Nil);
        } else {
                my $err = $@;
                chomp($err);
                set(intern($n_last_error), $err);
                return $Nil;
        }
}

sub Bstring_less {
        my ($prev, @rest) = checkargs($_[0], 'SR');
        while (@rest) {
                my $next = princs(shift(@rest));
                return $Nil unless $prev lt $next;
                $prev = $next;
        }
        return $T;
}

sub Bstring_equal {
        my ($first, @rest) = checkargs($_[0], 'SR');
        while (@rest) {
                return $Nil unless princs(shift(@rest)) eq $first;
        }
        return $T;
}

sub Bre_match {
        my ($re, $string) = checkargs($_[0], 'SS');
        my @result = $string =~ /$re/;
        #debug("$string =~ /$re/ result: @result");
        return array2list(@result);
}

sub Bre_subst {
        my ($regexp, $subst, $string, $options) = checkargs($_[0], 'SSS:S');
        $_ = $string;
        $options = '' if is_nil($options);
        my $code = "s{$regexp}{$subst}$options";
        #warn("re-subst: >>$code<<");
        eval($code);
        error($@) if $@;
        return $_;
}

sub Bconcat {
        my ($arglist) = @_;
        my $result = "";
        while (consp($arglist)) {
                my $arg;
                ($arg, $arglist) = cxr($arglist);
                $result .= princs($arg);
        }
        return $result;
}

sub Berror {
        my ($format, @args) = checkargs($_[0], 'SR');
        error($format, @args);
        return $Nil;                    # not reached
}

sub Bdoc {
        my ($funob, $brief, $noprint) = checkargs($_[0], 'e:ee');
        my $func = evalfun($funob);
        my $doc = function_documentation($func);
        my $name = symbol_name(function_name($func));
        my $type = function_type($func);
        my $special = specialp($func);
        my $desc = ($type eq 'expr' ? '' : 'builtin ')
                   . ($special ? 'special form' : 'function');
        my $args =  princs(function_params($func));
        my $result = "$desc: $name";
        $result .= " " . $args if $args;
        $result .= "\n" . $doc if $doc && is_nil($brief);
        if (!is_nil($noprint)) {
                return $result;
        } else {
                say($result);
                return $T;
        }
}

sub Bprinc {
        my ($arg) = checkargs($_[0], 'e');
        princ($arg);
        return $arg;
}

sub Bprincs {
        my ($arg) = checkargs($_[0], 'e');
        return princs($arg);
}

sub Bprin1 {
        my ($arg) = checkargs($_[0], 'e');
        prin1($arg);
        return $arg;
}

sub Bprint {
        my ($arg) = checkargs($_[0], 'e');
        Lprint($arg);
        return $arg;
}

sub Bterpri {
        checkargs($_[0], '');
        terpri();
        return $Nil;
}


sub Blabels {
        my ($arglist) = @_;
        my ($defs, @body) = checkargs($arglist, 'lR');
        my @bind_symbols = ();
        my @saved_funcs = ();

        while (consp($defs)) {
                my $def;
                ($def, $defs) = cxr($defs);
                error("malformed labels binding: %s", $def)
                    unless consp($def);
                my $var = is_sym(car($def));
                my $params = is_list(cadr($def));
                my $body = is_list(cddr($def));
                my $func = make_lambda($params, $body, 0, $var);
                
                push(@bind_symbols, $var);
                push(@saved_funcs, symbol_function($var));

                fset($var, $func);
                #debug("bind symbol %s to function %s", $var, $func);
        }
        my $result;
        # evaluate body forms
        while (@body) {
                $result = eval { Eval(shift(@body)); };
                last unless defined($result);
        }
        # restore bindings before throwing error
        for my $sym (@bind_symbols) {
                my $func = shift(@saved_funcs);
                #debug("restore symbol %s to function %s", $sym, $func);
                fset($sym, $func);
        }
	error ($@) if $@;
        return $result;
}

sub Blet_star {
        my ($arglist) = @_;
        my ($defs, @body) = checkargs($arglist, 'lR');
        my $n_bindings = 0;

        #debug("Blet_star %s", $defs);
        my $saved_env = enter_environment();
        my $result = eval {
                my $result = $Nil;
                while (consp($defs)) {
                        my $def;
                        ($def, $defs) = cxr($defs);
                        my $var;
                        my $value;
                        my $type = type_of($def);
                        if ($type eq "Symbol") {
                                $var = $def;
                                $value = $Nil;
                        } elsif ($type eq "Pair") {
                                $var = is_sym(car($def));
                                $value = Eval(is_def(cadr($def)));
                        } else {
                                error("malformed let* binding: %s", $def);
                        }
                        bind($var, $value);
                        #debug("bind symbol %s to %s", $var, $value);
                }
                # evaluate body forms
                while (@body) {
                        $result = Eval(shift(@body));
                }
                return $result;
        };
        backto_environment($saved_env);
	error ($@) if $@;
        return $result;
}

sub Blet {
        my ($arglist) = @_;
        my ($defs, @body) = checkargs($arglist, 'lR');
        my @bind_symbols = ();
        my @new_values = ();
        my $n_bindings = 0;

        #debug("Blet enter %s", $defs);
        my $saved_env = enter_environment();
        my $result = eval {
                my $result = $Nil;
                while (consp($defs)) {
                        my $def;
                        ($def, $defs) = cxr($defs);
                        my $var;
                        my $value;
                        my $type = type_of($def);
                        if ($type eq "Symbol") {
                                $var = $def;
                                $value = $Nil;
                        } elsif ($type eq "Pair") {
                                $var = is_sym(car($def));
                                $value = Eval(is_def(cadr($def)));
                        } else {
                                error("malformed let binding: %s", $def);
                        }
                        push(@bind_symbols, $var);
                        push(@new_values, $value);
                        #debug("bind symbol %s to %s", $var, $value);
                }
                for my $sym (@bind_symbols) {
                        bind($sym, shift(@new_values));
                }
                # evaluate body forms
                while (@body) {
                        $result = Eval(shift(@body));
                }
                return $result;
        };
        backto_environment($saved_env);
	error ($@) if $@;
        return $result;
}

sub Bload {
        my ($fname, $noerror, $nomessage) = checkargs($_[0], 'S:ee');
        return load($fname, $noerror, $nomessage);
}

sub Bsymbols {
        checkargs($_[0], '');
        return array2list(all_symbols);
}

sub Beq {
        my ($arg1, $arg2) = checkargs($_[0], 'ee');
        my $type1 = ref($arg1);
        my $type2 = ref($arg2);

        my $result;
        if ($type1 ne $type2) {
                $result = $Nil;
        } elsif ($type1 eq "") {
                $result = tornil($arg1 eq $arg2);
        } else {
                $result = tornil($arg1 == $arg2);
        }
        # debug("eq %s %s / %s %s -> %s",
        #       $arg1, $arg2, "$arg1", "$arg2", $result);
        return $result;
}

sub Bif {
        my ($condexpr, $thenclause, @elseclauses) = checkargs($_[0], 'eeR');
        my $result = $Nil;
        #debug("if is the biff");
        if (!is_nil(Eval($condexpr))) {
                #debug("if evals then clause %s", $thenclause);
                $result = Eval($thenclause);
        } else {
                for my $clause (@elseclauses) {
                        #debug("if evals else clause %s", $clause);
                        $result = Eval($clause);
                }
        }
        return $result;
}

sub Blambda {
        my ($params, $body) = checkargs($_[0], 'lr');
        return make_lambda($params, $body, 0, $Lambda);
}

sub make_lambda {
        my ($params, $body, $name) = @_;
        my $doc  = car($body);
        if (stringp($doc)) {
                $body = cdr($body);
        } else {
                $doc = '';
        }
        return function(cons($params, $body), 0, $doc, $name);
}

sub Bdefun {
        my ($name, $params, $body) = checkargs($_[0], 'ylr');
        fset($name, make_lambda($params, $body, $name));
        return $name;
}

sub Bcar {
        my ($list) = checkargs($_[0], 'l');
        return car($list);
}

sub Bcdr {
        my ($list) = checkargs($_[0], 'l');
        return cdr($list);
}

sub Bintern {
        my ($arg) = checkargs($_[0], 's');
        return intern($arg);
}

sub Bcons {
        my ($car, $cdr) = checkargs($_[0], 'ee');
        return cons($car, $cdr);
}

sub Blistp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(listp($arg));
}

sub Bsymbolp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(symbolp($arg));
}

sub Bnumberp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(numberp($arg));
}

sub Bstringp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(stringp($arg));
}

sub Bconsp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(consp($arg));
}

sub Bfunctionp {
        my ($arg) = checkargs($_[0], 'e');
        return tornil(functionp($arg));
}

sub Bsymbol_name {
        my ($arg) = checkargs($_[0], 'y');
        return symbol_name($arg);
}

sub Bsymbol_function {
        my ($arg) = checkargs($_[0], 'y');
        return symbol_function($arg);
}

sub Brplaca {
        my ($arg1, $arg2) = checkargs($_[0], 'pe');
        return rplaca($arg1, $arg2);
}

sub Brplacd {
        my ($arg1, $arg2) = checkargs($_[0], 'pe');
        return rplacd($arg1, $arg2);
}

sub Bfset {
        my ($sym, $func) = checkargs($_[0], 'ye');
        return fset($sym, $func);
}

sub Bplus {
        my @args = checkargs($_[0], 'N');
        my $sum = 0;
        while (@args) {
                $sum += pop(@args);
        }
        return $sum;
}

sub Bminus {
        my ($arg1, @more_args) = checkargs($_[0], 'nN');
        return -$arg1 unless @more_args;
        my $result = $arg1;
        while (@more_args) {
                $result -= pop(@more_args);
        }
        return $result;
}

sub Bmodulo {
        my ($start, @more_args) = checkargs($_[0], 'nN');
        while (@more_args) {
                $start %= pop(@more_args);
        }
        return $start;
}

sub Bproduct {
        my (@args) = checkargs($_[0], 'N');
        my $prod = 1;
        while (@args) {
                $prod *= pop(@args);
        }
        return $prod;
}

sub Bpower {
        my ($base, @more_args) = checkargs($_[0], 'nN');
        while (@more_args) {
                $base **= pop(@more_args);
        }
        return $base;
}

sub Bdivide {
        my ($start, @more_args) = checkargs($_[0], 'nN');
        unless (@more_args) {
                return 1 / $start;
        }
        while (@more_args) {
                $start /= pop(@more_args);
        }
        return $start;
}

sub Bdiv {
        my ($start, @more_args) = checkargs($_[0], 'nN');
        unless (@more_args) {
                return 1 / $start;
        }
        while (@more_args) {
                $start = int($start / pop(@more_args));
        }
        return $start;
}

sub Band {
        my ($arglist) = @_;
        my $lastval;
        while (consp($arglist)) {
                my $arg;
                ($arg, $arglist) = cxr($arglist);
                my $value = Eval($arg);
                return $Nil if is_nil($value);
                $lastval = $value;
        }
        return $lastval;
}

sub Bor {
        my ($arglist) = @_;
        while (consp($arglist)) {
                my $arg;
                ($arg, $arglist) = cxr($arglist);
                my $value = Eval($arg);
                return $value if !is_nil($value);
        }
        return $Nil;
}

sub Bfunction {
        my ($arg) = checkargs($_[0], 'e');
        return evalfun($arg);
}

sub Bquote {
        my ($arg) = checkargs($_[0], 'e');
        return $arg;
}

sub Bdump {
        my ($arg) = checkargs($_[0], 'e');
        return Dumper($arg);
}

sub Bperl {
        my ($expr) = checkargs($_[0], 'S');
        my $result = eval $expr;
        return $result;
}

sub Bnum_equal {
        my ($val1, @args) = checkargs($_[0], 'nN');
        while (defined(my $val2 = pop(@args))) {
                return $Nil unless $val1 == $val2;
        }
        return $T;
}

sub Bnum_less {
        my ($val1, @args) = checkargs($_[0], 'nN');
        while (defined(my $val2 = pop(@args))) {
                return $Nil unless $val1 < $val2;
                $val1 = $val2;
        }
        return $T;
}

sub Bwhile {
        my ($cond, $bodyforms) = checkargs($_[0], 'er');

        while (!is_nil(Eval($cond))) {
                eval_forms($bodyforms);
        }
        return $Nil;
}

sub Bset {
        my ($symbol, $value) = checkargs($_[0], 'ye');
        return set($symbol, $value);
}

sub Bsetq {
        my ($symbol, $value) = checkargs($_[0], 'ye');
        my $evalue = Eval($value);
        return set($symbol, $evalue);
}

sub Bfuncall {
        my ($func, $args) = checkargs($_[0], 'er');
        return funcall(evalfun($func), $args);
}

# snatched from lingo
sub spread_arglist {
        my ($arglist) = @_;
        my ($last, $last2);
        my $next = $arglist;
        while (consp($next)) {
                $last2 = $last;
                $last = $next;
                $next = cdr($next);
        }
        if (defined($last2)) {
                rplacd($last2, car($last));
        } else {
                $arglist = car($last);
        }
        return $arglist;
}

sub Bapply {
        my ($func, $arg1, $args) = checkargs($_[0], 'eer');
        my $last;                       # last pair of args
        my $last2;                      # 2nd but last
        if (is_nil($args)) {
                error("last arg of apply must be list") unless listp($arg1);
                $args = $arg1;
        } else {
                $args = spread_arglist(cons($arg1, $args));
        }
        return funcall(evalfun($func), $args);
}

sub Beval {
        my ($expr) = checkargs($_[0], 'e');
        my $evalue = Eval($expr);
        return $evalue;
}

sub Bnull {
        my ($arg) = checkargs($_[0], 'e');
        return $T if is_nil($arg);
        return $Nil;
}

sub Bdebug {
        my ($arg) = checkargs($_[0], ':e');
        unless (is_nil($arg)) {
                if (numberp($arg)) {
                        debug_level($arg);
                } elsif (is_nil($arg)) {
                        debug_level(0);
                } else {
                        debug_level(1);
                }
        }
        return debug_level();
}

sub Bboundp {
        my ($sym) = checkargs($_[0], 'y');
        return tornil(defined(symbol_value($sym)));
}

sub Bfboundp {
        my ($sym) = checkargs($_[0], 'y');
        return tornil(defined(symbol_function($sym)));
}

sub Bmakunbound {
        my ($sym) = checkargs($_[0], 'y');
        set($sym, undef);
        return $sym;
}

sub Bfmakunbound {
        my ($sym) = checkargs($_[0], 'y');
        fset($sym, undef);
        return $sym;
}

sub Btype_of {
        my ($ob) = checkargs($_[0], 'e');
        my $type = type_of($ob);
        return intern(lc($type)) unless $type eq "scalar";
        
        if (numberp($ob)) {
                return $t_Number;
        } else {
                return $t_String;
        }
}

my @builtins =                          # [name, func, is_special, doc]
    (
     ["%", \&Bmodulo, 0,
      "return remainder of NUM1 divided by NUM2 (which must be integers)"],
     ["*", \&Bproduct, 0,
      "return the product of all numbers"],
     ["**", \&Bpower, 0,
      "return BASE raised to the power POWER"],
     ["+", \&Bplus, 0,
      "return the sum of all argument numbers"],
     ["-", \&Bminus, 0,
      "return NUM1 minus all other numbers, or the negation of sole arg NUM1"],
     ["/", \&Bdivide, 0,
      "return NUM1 divided by all numbers, or the inverse of sole arg NUM1"],
     ["<", \&Bnum_less, 0,
      "return t if the arguments are strictly increasing, nil else"],
     ["=", \&Bnum_equal, 0,
      "return t all arguments are the same number, nil else"],
     ["and", \&Band, 1,
      "evaluate ARGS until one is nil and return the last evaluated value"],
     ["apply", \&Bapply, 0,
      "apply FUNCTION to ARGLIST"],
     ["boundp", \&Bboundp, 0,
      "return t if a value is bound to SYMBOL, nil otherwise"],
     ["car", \&Bcar, 0,
      "return the car of PAIR"],
     ["cdr", \&Bcdr, 0,
      "return the cdr of PAIR"],
     ["concat", \&Bconcat, 0,
      "concatenate the arguments to a string"],
     ["cond", \&Bcond, 1,
      "eval car of clauses until one is true, then eval rest of the clause"],
     ["cons", \&Bcons, 0,
      "return a new pair from elements CAR and CDR"],
     ["consp", \&Bconsp, 0,
      "return t if OBJECT is a pair, otherwise nil"],
     ["debug", \&Bdebug, 0,
      "set or get debug level"],
     ["decf", \&Bdecf, 1,
      "decrement number VAR by DELTA (or 1) and return the new value"],
     ["defun", \&Bdefun, 1,
      "define function with NAME, ARGS, and clauses"],
     ["defvar", \&Bdefvar, 1,
      "Define global variable SYMBOL with optional INITVALUE and DOCSTRING"],
     ["describe", \&Bdescribe, 0,
      "return list of OBJECT's type and features"],
     ["div", \&Bdiv, 0,
      "integer divide the first arg by all others"],
     ["doc", \&Bdoc, 0,
      "return the function documentation of ARG"],
     ["dolist", \&Bdolist, 1,
      "iterate over list (dolist (loopvar listform [resultform]) . bodyforms)"],
     ["dump", \&Bdump, 0,
      "return a dump of the internal data structure of the argument"],
     ["env-ref", \&Benv_ref, 0,
      "in ENV, look up SYMBOL, &optional not in NOPARENTS"],
     ["env-vars", \&Benv_vars, 0,
      "show all vars in ENV, &optional not in NOPARENTS"],
     ["environment", \&Benvironment, 0,
      "bindings of ENVIRON (or current) as list of pairs, &optional NOPARENTS"],
     ["eq", \&Beq, 0,
      "return t if ARG1 and ARG2 are the same object, nil else"],
     ["error", \&Berror, 0,
      "throw an error with MESSAGE and optional arguments"],
     ["errset", \&Berrset, 1,
      "return value of EXPR as singleton list, or nil in case of error"],
     ["eval", \&Beval, 0,
      "evaluate EXPRESSION"],
     ["exit", \&Bexit, 0,
      "exit Lis.pl with STATUS"],
     ["fboundp", \&Bfboundp, 0,
      "return t if a function is bound to SYMBOL, nil otherwise"],
     ["fmakunbound", \&Bfmakunbound, 0,
      "make SYMBOL's function definition be undefined"],
     ["format", \&Bformat, 0,
      "to CHANNEL, use FORMAT to format the remaining aruments"],
     ["fset", \&Bfset, 0,
      "set function cell of SYMBOL to FUNC and return FUNC"],
     ["funcall", \&Bfuncall, 0,
      "call FUNCTION with (remaining) arguments"],
     ["function", \&Bfunction, 1,
      "return the argument as a function"],
     ["function-environment", \&Bfunction_environment, 0,
      "return the environment of the specified FUNCTION"],
     ["functionp", \&Bfunctionp, 0,
      "return t if OBJECT is a function, otherwise nil"],
     ["get", \&Bget, 0,
      "from OBJECT's plist, get value of property INDICATOR"],
     ["glob-filenames", \&Bglob_filenames, 0,
      "return the list of file names matching PATTERN"],
     ["if", \&Bif, 1,
      "if COND is true, evaluate THEN-CLAUSE, otherwise all other clauses"],
     ["incf", \&Bincf, 1,
      "increment number VAR by DELTA (or 1) and return the new value"],
     ["intern", \&Bintern, 0,
      "return symbol with name STRING"],
     ["labels", \&Blabels, 1,
      "(labels ((sym args &rest body) ...) &rest body), define local function"],
     ["lambda", \&Blambda, 1,
      "define an anonymous function from ARGS and &rest BODY"],
     ["let", \&Blet, 1,
      "evaluate body with local bindings: (let ((var value) ...) body)"],
     ["let*", \&Blet_star, 1,
      "evaluate body with local bindings: (let* ((var value) ...) body)"],
     ["listp", \&Blistp, 0,
      "return t if OBJECT is a list, 0, otherwise nil"],
     ["load", \&Bload, 0,
      "load FILE, optional args NOERROR, NOMESSAGE"],
     ["makunbound", \&Bmakunbound, 0,
      "make SYMBOL's value be undefined"],
     ["null", \&Bnull, 0,
      "return t if the argument is nil, nil otherwise"],
     ["numberp", \&Bnumberp, 0,
      "return t if OBJECT is a number, otherwise nil"],
     ["or", \&Bor, 1,
      "evaluate ARGS until one is non-nil and return the last evaluated value"],
     ["perl", \&Bperl, 0,
      "evaluate the ARG string as Perl code and return the result"],
     ["pop", \&Bpop, 1,
      "remove the first item of list in VAR, store changed VAR, return item"],
     ["prin1", \&Bprin1, 0,
      "print ARG suitable for read"],
     ["princ", \&Bprinc, 0,
      "print ARG to standard output without quoting"],
     ["princs", \&Bprincs, 0,
      "princ ARG to a string and return the string"],
     ["print", \&Bprint, 0,
      "print newline, ARG suitable for read, and space"],
     ["push", \&Bpush, 1,
      "prepend ITEM to the list in VAR and store the result in VAR"],
     ["put", \&Bput, 0,
      "on OBJECT's plist set property INDICATOR to VALUE"],
     ["quote", \&Bquote, 1,
      "return the argument without evaluating it"],
     ["random", \&Brandom, 0,
      "return a random number a with 0 <= a < LIMIT (or 1)"],
     ["re-match", \&Bre_match, 0,
      "match REGEXP to a string and return the result(s)"],
     ["re-subst", \&Bre_subst, 0,
      "match REGEXP to a string, substitute by SUBST, and return the result"],
     ["read", \&Bread, 0,
      "return an expression read from stdin or &optional INPUT (a string)"],
     ["remprop", \&Bremprop, 0,
      "from SYMBOL's plist, remove property INDICATOR"],
     ["rplaca", \&Brplaca, 0,
      "set the car of PAIR to NEWCAR and return NEWCAR"],
     ["rplacd", \&Brplacd, 0,
      "set the cdr of PAIR to NEWCDR and return NEWCDR"],
     ["set", \&Bset, 0,
      "set SYMBOL's value as a variable to VALUE, return VALUE"],
     ["setq", \&Bsetq, 1,
      "set (implicitly quoted) VARIABLE to VALUE"],
     ["shell", \&Bshell, 0,
      "run COMMAND as a shell command; &optional RETURN-OUTPUT"],
     ["split-string", \&Bsplit_string, 0,
      "split STRING into parts SEPARATED by SEP and return the list"],
     ["sqrt", \&Bsqrt, 0,
      "return the square root of numeric ARG"],
     ["string<", \&Bstring_less, 0,
      "return t if argument strings are in strictly ascending order, nil else"],
     ["string=", \&Bstring_equal, 0,
      "return t if all arguments are equal as strings, nil else"],
     ["stringp", \&Bstringp, 0,
      "return t if OBJECT is a string, otherwise nil"],
     ["sublist", \&Bsublist, 0,
      "return sublist of LIST beginning at START ending at END (zero-based)"],
     ["substr", \&Bsubstr, 0,
      "return the substring of STRING from START to &optional END"],
     ["symbol-function", \&Bsymbol_function, 0,
      "return the value of the function cell of SYMBOL"],
     ["symbol-name", \&Bsymbol_name, 0,
      "return the name of SYMBOL as a string"],
     ["symbol-plist", \&Bsymbol_plist, 0,
      "return SYMBOL's plist"],
     ["symbolp", \&Bsymbolp, 0,
      "return t if OBJECT is a symbol, otherwise nil"],
     ["symbols", \&Bsymbols, 0,
      "return a list of all symbols"],
     ["terpri", \&Bterpri, 0,
      "terminate a print line with a newline char"],
     ["the-environment", \&Bthe_environment, 0,
      "return the current environment object"],
     ["truncate", \&Btruncate, 0,
      "return NUMBER truncated to integer towards zero"],
     ["type-of", \&Btype_of, 0,
      "return type of OBJECT as a symbol"],
     ["unless", \&Bunless, 1,
      "if COND yields nil, eval BODYFORMS and return the result of the last"],
     ["when", \&Bwhen, 1,
      "if COND yields true, eval BODYFORMS and return the result of the last"],
     ["while", \&Bwhile, 1,
      "while COND is true, eval &rest CLAUSES"],
    );

sub init {
        for my $b (@builtins) {
                my ($name, $func, $is_special, $doc) = @$b;
                error("Builtin::init: malformed function descriptor\n"
                      . Dumper($b))
                    unless @$b == 4;
                my $namesym = intern($name);
                fset($namesym, function($func, $is_special, $doc, $namesym));
                #warn("defined builtin $name");
        }
}

1;
