# Global variables, some to ease interdependency problems

package Global;

use warnings;
use strict;
use 5.014;
use Carp;

#use Debug;

use vars qw(@ISA @EXPORT);
use Exporter ();

my $debugfh = \*STDOUT;
BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw($Nil $T $n_Symbol $n_Pair $n_Function $n_function $special
                     $builtin $function $n_last_eval_stats
                     $n_Quote error $Leval $andRest $Lambda $f_Princs
                     $n_last_error
                     $n_ARGS our $t_Symbol $t_Number $t_String $t_Pair
                     $t_Function $opt_stacktraces
                     $andOptional $n_Environment $n_parentenv
                     $n_root_environment $root_environment
                     $n_environment_level $n_env_name $eval_depth
            );
}

# depth of the eval stack
our $eval_depth = 0;

# some symbols
our $Nil;                               # the nil symbol
our $T;                                 # the t symbol
our $andRest;                           # the &rest token symbol
our $andOptional;                       # the &optional token symbol
our $Lambda;                            # the lambda symbol
our $builtin;
our $function;
our $root_environment;                  # symbol *root-environment*

# object types, all Symbols
our $t_Symbol;
our $t_Number;
our $t_String;
our $t_Pair;
our $t_Function;
our $t_Environment;

# a few symbol names
our $n_Symbol = 'Symbol';
our $n_Pair = 'Pair';
our $n_Function = 'Function';
our $n_function = 'function';
our $n_last_error = '*last-error*';
our $n_last_eval_stats = '*last-eval-stats*';
our $n_ARGS = '*ARGS*';
our $n_Environment = 'Environment';
our $n_root_environment = '*root-environment*';
our $n_env_name = '*env-name*';
our $n_parentenv = '*parent-environment*';
our $n_Quote = 'quote';

# global function (value set in Print init)
our $f_Princs;                          # the printer function

# global option variable(s)
our $opt_stacktraces = 0;               # print stacktraces with errors


# for some weird reason I cannot take a \&die reference
sub do_die {
        die(@_);
}

sub error {
        my ($msg, @data) = @_;
        my $reporter = $opt_stacktraces ? \&confess : \&do_die;
        @data = map {&$f_Princs($_)} @data;
        $msg .= "\n"  unless $msg =~ /\n$/;
        &$reporter(sprintf($msg, @data));
}

1;
