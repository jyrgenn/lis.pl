package Debug;

use warnings;
use strict;
use 5.014;

use Print;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( debug debug_level debug_stacktrace );
}

my $debug_level = 0;

sub debug_level {
        my ($newlevel) = @_;
        if (defined($newlevel)) {
                $debug_level = $newlevel;
        }
        return $debug_level;
}

sub debug {
        if ($debug_level) {
                my ($format, @args) = @_;
                chomp($format);
                $format .= "\n";
                printf($format, map { princs($_); } @args);
        }
}

sub debug_stacktrace {
        return if $debug_level <= 3;
        my $frame_no = 0;
        my @frame = ();
        while (my @data = caller($frame_no)) {
               $frame[$frame_no] = \@data;
                say(3, "STACK:", join(":", $data[3], $frame[$frame_no-1][2]))
                  if $frame_no;         # we need the $line from stack frame 0
                                        # (which is where debug_stacktrace()
                                        # was called from), but nothing else
                                        # from that frame
                $frame_no++;
        }
}

1;
