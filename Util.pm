# Utility functions

package Util;

use warnings;
use strict;
use 5.014;

use Data::Dumper;

use Global;
use Object;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( is_def is_sym is_list tornil array2list list2array
                      checkargs
                    );
}


sub caller_name {
        my ($level) = @_;
        my (undef, undef, undef, $name) = caller($level + 1);
        $name =~ s/.*B//;
        return $name;
}

sub arg_type_error {
        my ($argno, $type, $arg) = @_;
        error("argument %d to builtin function %s is not a %s: %s",
              $argno, caller_name(2), $type, $arg);
}

# check arguments; descriptor is string "xxx:xx" with mandatory args
# before the :, then optional args; x is:
#  e: any expression
#  E: environment
#  y: symbol
#  p: pair
#  l: list (cons or nil)
#  n: number
#  s: string
#  S: convert to string
#  r: rest of the arglist as list
#  R: rest of the arglist converted to array
#  N: rest of the arglist as numbers, converted to array
sub checkargs {
        my ($arglist, $descriptor) = @_;
        my @result = ();
        my $argno = 0;
        my $optional = 0;
        for my $desc (split('', $descriptor)) {
                #warn("checkargs: desc $desc; arglist ", &$f_Princs($arglist));
                if ($desc eq 'R') {
                        #warn("checkargs: set $desc to ", &$f_Princs($arglist));
                        push(@result, list2array($arglist));
                        last;
                }
                if ($desc eq 'N') {
                        while (consp($arglist)) {
                                my $arg;
                                ($arg, $arglist) = cxr($arglist);
                                $argno++;
                                arg_type_error($argno, "number", $arg)
                                    unless numberp($arg);
                                push(@result, $arg);
                        }
                        last;
                }
                if ($desc eq 'r') {
                        #warn("checkargs: set $desc to ", &$f_Princs($arglist));
                        push(@result, $arglist);
                        last;
                }
                if ($desc eq ':') {
                        #warn("checkargs: see :optional");
                        $optional = 1;
                        next;
                }
                error("too few arguments to function %s: %s",
                      caller_name(1), $argno)
                    unless $optional || consp($arglist);
                my $arg;
                ($arg, $arglist) = cxr($arglist);
                #warn("\$arg: ", Dumper($arg));
                my $optional_defaulted = 0;
                if (defined($arg)) {
                        $argno++;
                } else {
                        $arg = $Nil;
                        $optional_defaulted = 1;
                }
                if ($desc eq 'e' || $optional_defaulted) {
                        ;               # this is relatively frequent, and we do
                                        # not need to check the argument type
                                        # any more
                } elsif ($optional && is_nil($arg)) {
                        ;               # explicit nil arg for optional param is
                                        # ok
                } elsif ($desc eq 'S') {
                        $arg = &$f_Princs($arg);
                } elsif ($desc eq 'y') {
                        arg_type_error($argno, "symbol", $arg)
                            unless symbolp($arg);
                } elsif ($desc eq 'p') {
                        arg_type_error($argno, "pair", $arg)
                            unless consp($arg);
                } elsif ($desc eq 'l') {
                        arg_type_error($argno, "list", $arg)
                            unless listp($arg);
                } elsif ($desc eq 'n') {
                        arg_type_error($argno, "number", $arg)
                            unless numberp($arg);
                } elsif ($desc eq 's') {
                        arg_type_error($argno, "string", $arg)
                            unless stringp($arg);
                } elsif ($desc eq 'E') {
                        arg_type_error($argno, "string", $arg)
                            unless environmentp($arg);
                } else {
                        error("internal error, unknown arg descriptor %s",
                              $desc);
                }
                #warn("checkargs: set $desc to ", &$f_Princs($arg));
                push(@result, $arg);
        }
        #warn("checkargs: return ", &$f_Princs(array2list(@result)));
        return @result;
}

sub array2list {
        my (@elems) = @_;
        my $result = $Nil;
        while (@elems) {
                my $elem = pop(@elems);
                $result = cons($elem, $result);
        }
        return $result;
}

sub list2array {
        my ($list) = @_;
        my @result = ();
        while (consp($list)) {
                push(@result, car($list));
                $list = cdr($list);
        }
        return @result;
}

# value is defined
sub is_def {
        my ($arg) = @_;
        error("undefined argument") unless defined($arg);
        return $arg;
}

# value is defined and a symbol
sub is_sym {
        my ($arg) = @_;
        error("undefined argument") unless defined($arg);
        error("not a symbol: %s", $arg) unless symbolp($arg);
        return $arg;
}

# value is defined and a list
sub is_list {
        my ($arg) = @_;
        error("undefined argument") unless defined($arg);
        error("not a list: %s", $arg) unless listp($arg);
        return $arg;
}

# convert Perl truth value to Lisp t or nil
sub tornil {
        my ($arg) = @_;
        if ($arg && $arg != $Nil) {
                return $T;
        } else {
                return $Nil;
        }
}

1;
