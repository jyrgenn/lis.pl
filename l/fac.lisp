(defun facr (n)
  (if (< n 2)
      1
    (* n (facr (1- n)))))

(defun fach (n)
  (labels ((helper (n acc)
                   (if (<= n 1)
                       acc
                     (helper (1- n) (* n acc)))))
    (helper n 1)))

(defun facw (n)
  (let ((result 1))
    (while (< 1 n)
      (setq result (* n result))
      (decf n))
    result))

(defun make-counter (&optional init)
  (let ((next (or init 1)))
    (lambda ()
      (prog1
          next
        (incf next)))))

(defun facl (n)
  (apply #'* (make-list n (make-counter))))
