    (let ((a 1))                            ; binding (1)
      (let ((f (lambda () (print a))))
        (let ((a 2))                        ; binding (2)
          (funcall f))))
