
(defun fib (n)
  "Return the Fibonacci number N."
  (if (< n 2)
      1
    (+ (fib (- n 1))
       (fib (- n 2)))))

