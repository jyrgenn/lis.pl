(defun lslice (l from to)
  "Return a slice of list L, beginning at FROM, ending just before TO.
If one or both of the indexes are outside of the list, return a shorter
list as appropriate."
  (let ((result '())
        last-pair
        (index 0))
    (while (and l (< index from))
      (setq l (cdr l))
      (setq index (1+ index)))
    (while (and l (< index to))
      (let ((newpair (cons (pop l) nil)))
        (if result
            (rplacd last-pair newpair)
          (setq result newpair))
        (setq last-pair newpair))
      (setq index (1+ index)))
    result))

(defun lslice-r (l from to)
  "Return a slice of list L, beginning at FROM, ending just before TO.
If one or both of the indexes are outside of the list, return a shorter
list as appropriate."
  (if (null l)
      nil
    (if (< to 1)
        nil
      (if (< from 1)
          (cons (car l) (lslice-r (cdr l) from (1- to)))
        (lslice-r (cdr l) (1- from) (1- to))))))

