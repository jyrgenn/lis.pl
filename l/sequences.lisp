(defun sequencep (object)
  "return t if OBJECT is a sequence (list or string), nil else"
  (or (listp object) (stringp object)))

(defun subseq (seq start &optional end)
  "return a copy of the subsequence of SEQ bounded by START and END"
  (cond ((listp seq) (sublist seq start end))
        ((stringp seq) (substr seq start end))
        (t (error "subseq: not a sequence: %s" seq))))

(defun length (sequence)
  "return the length of SEQUENCE"
  (if (stringp sequence)
      (setq sequence (split-string sequence "")))
  (if (null sequence)
      0
    (1+ (length (cdr sequence)))))

(defun first (sequence)
  "return the first element of SEQUENCE"
  (cond ((stringp sequence) (substr sequence 0 1))
        ((listp sequence) (car sequence))
        (t (error "first: not a sequence: %s" sequence))))

(defun rest (sequence)
  "return all but the first element of SEQUENCE"
  (cond ((stringp sequence) (substr sequence 1))
        ((listp sequence) (cdr sequence))
        (t (error "rest: not a sequence: %s" sequence))))

(defun prepend (element sequence)
  "prepend ELEMENT to SEQUENCE and return the resulting sequence"
  (cond ((stringp sequence) (string element sequence))
        ((listp sequence) (cons element sequence))
        (t (error "prepend: not a sequence: %s" sequence))))

(defun empty (sequence)
  "return non-nil iff SEQUENCE is empty"
  (cond ((stringp sequence) (string= "" sequence))
        ((listp sequence) (null sequence))
        (t (error "empty: not a sequence: %s" sequence))))

(defun sort-string (s pred)
  "sort string S with predicate PRED and return the resulting string"
  (if (or (string= s "") (= 1 (strlen s)))
      s
    (let* ((len (strlen s))
           (half (div len 2))
           (s1 (substr s 0 half))
           (s2 (substr s half))
           (merge (lambda (s1 s2)
                    (if (string= "" s1)
                        s2
                      (if (string= "" s2)
                          s1
                        (if (pred (substr s1 0 1) (substr s2 0 1))
                            (string (substr s1 0 1) (merge (substr s1 1) s2))
                          (string (substr s2 0 1) (merge s1 (substr s2 1)))))))))
      (merge (sort-string s1 pred) (sort-string s2 pred)))))

(defun sort-list (l pred)
  "sort list L with predicate PRED and return the resulting list"
  (if (or (null l) (null (cdr l)))
      l
    (let* ((len (length l))
           (half (div len 2))
           (l1 (sublist l 0 half))
           (l2 (sublist l half))
           (merge (lambda (l1 l2)
                    (if (null l1)
                        l2
                      (if (null l2)
                          l1
                        (if (pred (car l1) (car l2))
                            (cons (car l1) (merge (cdr l1) l2))
                          (cons (car l2) (merge l1 (cdr l2)))))))))
      (merge (sort-list l1 pred) (sort-list l2 pred)))))

(defun sort (s pred)
  "sort sequence S with predicate PRED and return the result"
  (cond ((stringp s)
         (apply #'string (sort-list (split-string s "") pred)))
        ((listp s) (sort-list s pred))
        (t (error "sort: not a sequence: %s" s))))
