# the Lisp printer

package Print;

use warnings;
use strict;
use 5.014;
use Scalar::Util qw(refaddr);

use Global;
use Object;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( terpri
                      prin1
                      princ
                      princs
                      Lprint
                    );
}

# output suitable for read
sub prin1 {
        my ($ob, $out) = @_;
        $out //= \*STDOUT;
        print $out (printob($ob, 1));
}

# output to look good
sub princ {
        my ($ob, $out) = @_;
        $out //= \*STDOUT;
        print $out (printob($ob, 0));
}

# output to string
sub princs {
        my ($ob) = @_;
        return printob($ob, 0);
}

# prin1 with newline and space
sub Lprint {
        my ($ob, $out) = @_;
        $out //= \*STDOUT;
        print $out ("\n", printob($ob, 1), " ");
}

sub terpri {
        my ($ob, $out) = @_;
        $out //= \*STDOUT;

        print $out ("\n");
}

# to a string; prin1-like iff $quote
sub printob {
        my ($ob, $quote) = @_;
        #warn("printob($ob, $out)");
        $ob //= "<##UNDEF##>";
        if (symbolp($ob)) {
                return symbol_name($ob);
        } elsif (consp($ob)) {
                return print_list($ob);
        } elsif (functionp($ob)) {
                return print_function($ob);
        } elsif (environmentp($ob)) {
                return print_environment($ob);
        } elsif (numberp($ob)) {
                return "$ob";
        } elsif ($quote) {              # is a string
                $ob =~ s/\\/\\\\/g;
                $ob =~ s/"/\\"/g;
                return qq{"$ob"};
        } else {
                return $ob;
        }
}

sub print_environment {
        my ($ob) = @_;
        my $result =  sprintf("#<Env-%x", refaddr($ob));
        # $result .= ':';
        # $result .= join(',', keys(%$ob));
        $result .= '>';
        return $result;
}

sub print_function {
        my ($ob) = @_;
        my $type = function_type($ob);
        my $code = function_code($ob);
        my $special = specialp($ob);
        if ($type eq 'subr') {
                my $namesym = function_name($ob);
                my $name = $namesym == $Nil ? '' : symbol_name($namesym);
                my $spectag = $special ? 'f' : '';
                return "#<$special$type:$name:$code>";
        } elsif ($type eq 'expr') {
                return "#'" . printob(cons($Lambda, $code));
        }
}

sub print_list {
        my ($ob) = @_;
        my $result = "(";

        my $first = 1;
        while (consp($ob)) {
                if ($first) {
                        $first = 0;
                } else {
                        $result .= " ";
                }
                $result .= printob(car($ob), 1);
                $ob = cdr($ob);
        }
        unless (is_nil($ob)) {
                $result .= " . ";
                $result .= printob($ob, 1);
        }
        $result .= ")";
        return $result;
}

sub init {
        $f_Princs = \&princs;
}

1;
