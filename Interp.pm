# Io -- the REPL and load, bascially
package Interp;

use strict;
use warnings;
use 5.014;

use Data::Dumper;
use Time::HiRes qw(time);

use Global;
use Object;
use Util;
use Read;
use Print;
use Eval;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( repl load eval_string
                    );
}

sub eval_string {
        my ($string, $quiet) = @_;
        return iEval(Read($string), $quiet);
}

sub iEval {
        my ($expr, $quiet) = @_;
        eval_level(0);
        eval_count(0);
        cons_count(0);

        my $start = time();
        my $value = eval { Eval($expr); };
        my $tdiff = time() - $start;
        my $evals = eval_count();
        my $pairs = cons_count();

        if (defined($value)) {
                Lprint($value);
                terpri();
        } else {
                say("Error: $@");
        }
        set(intern($n_last_eval_stats),
            array2list(intern("evals"), $evals,
                       intern("pairs"), $pairs,
                       intern("s"), $tdiff));
        printf(";; %d evals %d pairs in %d ms, %g evals/s\n",
               $evals, $pairs, int($tdiff * 1000),
               $tdiff ? $evals / $tdiff : "NaN")
            unless $quiet;
        return $value;
}

sub repl {
        my ($fh, $interactive) = @_;

        my $value = 0;
        while (1) {
                if ($interactive) {
                        print("> ");
                }
                my $expr = eval {
                        my $expr = Read($fh);
                        return $expr;
                };
                if ($@) {
                        say("Error: $@");
                        $value = undef;
                        next;
                } elsif (!defined($expr)) {
                        # undef + !$@ is EOF
                        last;
                }

                if ($interactive) {
                        $value = iEval($expr);
                        set(intern('*'), $value);
                } else {
                        $value = eval { Eval($expr); };
                        unless (defined($value)) {
                                say("Error: $@");
                                last;
                        }
                }
        }
        return $value;
}

sub load {
        my ($fname, $noerror, $nomessage) = @_;
        my $fh;

        unless (open($fh, "<:encoding(UTF-8)", $fname)) {
                unless ($noerror) {
                        say("cannot open file ($!): %s", $fname);
                        return $Nil;
                }
                error("cannot open file ($!) %s", $fname);
        }
        print(";; loading file $fname ... ") unless $nomessage;
        $| = 1;
        start_file($fname);
        my $result = repl($fh, 0);
        close($fh);
        end_file();
        if (defined($result) && !$nomessage) {
                say("done");
        }
        return $result;
}

1;
