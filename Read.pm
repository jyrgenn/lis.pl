# The reader

package Read;

use warnings;
use strict;
use 5.014;

use Scalar::Util qw(looks_like_number);

use Global;
use Debug;
use Object;

use vars qw(@ISA @EXPORT);
use Exporter ();

BEGIN {
        @ISA = qw(Exporter);
        @EXPORT = qw( Read start_file end_file
                   );
}

my $default_input = \*STDIN;

my $sepchars = quotemeta("();\n");

my $linecount;
my $filename;

sub start_file {
        my ($fname) = @_;
        $linecount = 1;
        $filename = $fname;
}

sub end_file {
        undef($linecount);
        undef($filename);
}

# after a read error, consume to end of line for the benefit of interactive use
sub read_error {
        my ($in, $format, @args) = @_;
        <$in>;
        if ($filename) {
                error("\n\n$filename:$linecount: $format\n", @args);
        } else {
                error($format, @args);
        }
}

sub Read {
        my ($in) = @_;
        $in //= $default_input;
        if (stringp($in)) {
                my $string = $in;
                open(my $str_in, "<", \$string) or
                    error("read: cannot open string as filehandle: $!");
                $in = $str_in;
        }

        my $t = next_token($in);
        #warn("next token: $t");
        return undef unless defined($t);
        if ($t eq '(') {
                my $list = read_list_elems($in);
                return undef unless defined($list);
                my $closer = next_token($in);
                read_error($in, "list closed by $closer") unless $closer eq ')';
                return $list;
        } elsif ($t eq '\'') {
                $t = Read($in);
                read_error($in, "EOF in quote") unless defined($t);
                return cons(intern($n_Quote), cons($t, $Nil));
        } elsif ($t eq '#\'') {
                $t = Read($in);
                read_error($in, "EOF in #quote") unless defined($t);
                return cons(intern($n_function), cons($t, $Nil));
        } elsif ($t eq '.') {
                read_error($in, "found . when expecting sexpr");
        } elsif (symbolp($t)) {
                return $t;
        } elsif ($t =~ m{^\"}) {
                return substr($t, 1);
        } elsif ($t eq ')') {
                read_error($in, "close paren unexpected");
        } else {
                return $t;
        }
}


sub read_list_elems {
        my ($in) = @_;
        my $list = $Nil;
        my $end;                        # last element of list, if defined
        my $t;
        while (defined($t = next_token($in))) {
                if ($t =~ /^[\]\}\)]$/) {
                        push_back_token($t);
                        return $list;
                } elsif ($t eq '.') {
                        read_error($in, ". at start of list") unless $end;
                        my $sexpr = Read($in);
                        return undef unless defined($t);
                        $t = next_token($in);
                        read_error($in, "no ) after end of improper list")
                            unless $t eq ')';
                        rplacd($end, $sexpr);
                        push_back_token($t);
                        return $list;
                } else {
                        push_back_token($t);
                        my $sexpr = Read($in);
                        return undef unless defined($sexpr);
                        my $newpair = cons($sexpr, $Nil);
                        if ($end) {
                                $end = rplacd($end, $newpair);
                        } else {
                                $list = $end = $newpair;
                        }
                }
        }
        read_error($in, "EOF reading list elements");
}


my $pushback_token;                     # next token to be deliverd by
                                        # next_token() if defined
sub push_back_token {
        my ($t) = @_;
        $pushback_token = $t;
}

sub next_token {
        my ($in) = @_;
        if (defined($pushback_token)) {
                my $result = $pushback_token;
                undef($pushback_token);
                return $result;
        }
        my $c = next_nonwhite($in);
        return undef unless defined($c);
        #warn("next nonwhite: >$c<");
        if ($c =~ /[$sepchars'`,@]/o) {
                return $c;
        } elsif ($c eq '#') {
                if ($in->input_line_number() == 1) {
                        $in->getline();
                        return next_token($in);
                }
                return read_macro($in);
        } elsif ($c eq '"') {
                return read_string($in);
        } else {
                $in->ungetc(ord($c));
                my $t = symbol_or_number_or_dot($in);
                return '.' if $t eq '.';
                return $t if looks_like_number($t);
                return intern($t);
        }
}

my %esc_seq = (a => "\a", b => "\b", e => "\0x1b", f => "\f", n => "\n",
               r => "\r", t => "\t", v => "\x0b");

# can read the above escaped chars, but no \OOO, \x00, \U yet
sub read_string {
        my ($in) = @_;
        my $s = '"';
        my $c;
        my $escaped = 0;                # numeric, for how many chars
        while (defined($c = $in->getc())) {
                inc_line() if $c eq "\n";
                if ($escaped) {
                        $escaped--;
                        $s .= $esc_seq{$c} // $c;
                } elsif ($c eq '\\') {
                        $escaped = 1;
                } elsif ($c eq '"') {
                        last;
                } else {
                        $s .= $c;
                }
        }
        error_or_eof($in) unless $c eq '"';
        return $s;
}

sub symbol_or_number_or_dot {
        my ($in) = @_;
        my $t = "";
        my $c;
        while (defined($c = $in->getc()) && $c !~ /[$sepchars\s]/o) {
                #warn("add to token >$c<");
                $t .= $c;
        }
        $in->ungetc(ord($c)) if defined($c);
        return $t;
}

sub error_or_eof {
        my ($in) = @_;
        if (defined($!)) {
                read_error($in, "read failure: $!");
        }
        read_error($in, "unexpected EOF");
}

sub read_macro {
        my ($in) = @_;
        #debug("read macro");
        my $c = next_nonwhite($in);
        read_error($in, "unexpected EOF") unless defined($c);
        if ($c eq '\'') {
                my $result = '#\'';
                #debug("read macro returns %s", $result);
                return $result;
        # and possibly more
        } else {
                read_error($in, "unknown reader macro \"#$c\"");
        }
}

sub inc_line {
        $linecount++ if $linecount;
}

sub next_nonwhite {
        my ($in) = @_;
        my $c;
        while (defined($c = $in->getc())) {
                inc_line() if $c eq "\n";
                next if $c =~ /\s/;
                if ($c eq ';') {        # skip comment
                        $in->getline();
                        inc_line();
                        next;
                }
                return $c;
        }
        return undef;
}

1;
