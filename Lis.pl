#!/usr/bin/perl

use strict;
use warnings;
use 5.014;

use Data::Dumper;

use Global;
use Util;
use Object;
use Builtin;
use Print;
use Interp;

# -e -h -l -q -x

my $interactive = 1;                    # run interactively; this gets switched
                                        # off by file and -e arguments
my $opt_quiet = 0;                      # be quiet
my $opt_interactive = 0;                # interactive despite file or -e arg
my $error = 0;                          # potential error exit

my @argv_save = @ARGV;

# first run through arguments -- we need these options before evaluating the
# other ones

 ARGS1:
    while ($ARGV[0] && $ARGV[0] =~ /^-/) {
            for (split(//, substr(shift(@ARGV), 1))) {
                    if (/-/) {
                            last ARGS1;
                    } elsif (/e/) {
                            shift(@ARGV)
                    } elsif (/h/) {
                            usage(0);
                    } elsif (/l/) {
                            shift(@ARGV)
                    } elsif (/i/) {
                            $opt_interactive = 1;
                    } elsif (/q/) {
                            $opt_quiet = 1;
                    } elsif (/s/) {
                            $opt_stacktraces = 1;
                    } else {
                            usage(1);
                    }
            }
}

Object::init();
Builtin::init();
Print::init();

defvar(intern($n_ARGS), $Nil, "command line arguments");
defvar(intern($n_last_eval_stats), $Nil, "statistics of last interactve eval");
defvar(intern('*'), $Nil, "last eval result in repl");
defvar(intern($n_last_error), $Nil, "last error message after errset");

load("Fundamental.lisp", 0, $opt_quiet) or exit(1);

@ARGV = @argv_save;
 ARGS2:
    while ($ARGV[0] && $ARGV[0] =~ /^-/) {
            for (split(//, substr(shift(@ARGV), 1))) {
                    if (/-/) {
                            last ARGS2;
                    } elsif (/e/) {
                            my $v = eval_string(shift(@ARGV) || usage(1),
                                                $opt_quiet);
                            $error = !defined($v);
                            $interactive = 0;
                    } elsif (/h/) {
                    } elsif (/l/) {
                            my $v = load(shift(@ARGV) || usage(1),
                                         0, $opt_quiet);
                            $error = !defined($v);
                    } elsif (/i/) {
                    } elsif (/q/) {
                    } elsif (/s/) {
                    } else {
                            usage(1);
                    }
            }
}

# first arg is a file to load
my $loadfile_arg = shift(@ARGV) // '';

# remaining args are *ARGS* for the application
set(intern($n_ARGS), array2list(@ARGV));

if ($loadfile_arg) {
        my $v = load($loadfile_arg);
        $error = !defined($v);
        $interactive = 0;
}

if ($interactive || $opt_interactive) {
        my $v = repl(\*STDIN, !$opt_quiet);
        print("\n");
        $error = !defined($v);
}
exit($error);

########

sub usage {
        my ($status) = @_;
        my $fh = $status ? \*STDERR : \*STDOUT;
        print $fh (<<EOU);
usage: Lis.pl [-hq] [-e expression] [-l loadfile] [file] [arg0 ...]
 -e: evaluate expression
 -h: show this help text
 -i: run interactively even with -e or file arguments
 -l: load file before starting repl
 -q: be quiet (and non-interactive in general)
 -s: show Perl stacktraces with errors
EOU
        exit($status);
}

# EOF
