;; fundamental predefined Lisp functions
;(debug t)

(defun max (first &rest numargs)
  "return the biggest of all (numeric) arguments"
  (if (null numargs)
      first
    (let ((next (apply #'max numargs)))
      (if (< first next)
          next
        first))))

(defun min (first &rest numargs)
  "return the smallest of all (numeric) arguments"
  (if (null numargs)
      first
    (let ((next (apply #'min numargs)))
      (if (< first next)
          first
        next))))

(defun apropos (match)
  (sort (filter #'(lambda (sym) (re-match match (symbol-name sym)))
                (symbols))
        #'string<))

(defun floor (number)
  "return NUMBER truncated toward negative infinity"
  (let ((trunc (truncate number)))
    (if (< number trunc)
        (1- trunc)
      trunc)))

(defun ceiling (number)
  "return NUMBER truncated toward positive infinity"
  (let ((trunc (truncate number)))
    (if (< trunc number)
        (1+ trunc)
      trunc)))

(defun fraction (number)
  "return the fractional part of NUMBER"
  (- number (truncate number)))

(defun integerp (obj)
  "return t if OBJ is an integer, nil else"
  (and (numberp obj)
       (= obj (truncate obj))))

(defun evenp (number)
  "return t if NUMBER is divisible by 2, else nil"
  (and (integerp number)
       (zerop (% number 2))))

(defun sign (number)
  "return -1, 0, or 1 if NUMBER is negative, zero, or positive"
  (cond ((< number 0) -1)
        ((= number 0) 0)
        (t 1)))

(defun round (number)
  "return NUMBER rounded to the nearest integer"
  (let ((frac (fraction number)))
    (if (= frac 0.5)
        (if (evenp number)
            number
          (+ number frac))
      (truncate (+ number (* (sign number) 0.5))))))

(defun isqrt (num)
  "return the integer square root of NUM ARG"
  (truncate (sqrt num)))

(defun caar (pair)
  "return the car of the car of PAIR"
  (car (car pair)))

(defun cadr (pair)
  "return the car of the cdr of PAIR"
  (car (cdr pair)))

(defun cdar (pair)
  "return the cdr of the car of PAIR"
  (cdr (car pair)))

(defun cddr (pair)
  "return the cdr of the cdr of PAIR"
  (cdr (cdr pair)))

(defun caaar (pair)
  "return the car of the car of the car of PAIR"
  (car (caar pair)))

(defun caadr (pair)
  "return the car of the car of the cdr of PAIR"
  (car (cadr pair)))

(defun cadar (pair)
  "return the car of the cdr of the car of PAIR"
  (car (cdar pair)))

(defun caddr (pair)
  "return the car of the cdr of the cdr of PAIR"
  (car (cddr pair)))

(defun cdaar (pair)
  "return the cdr of the car of the car of PAIR"
  (cdr (caar pair)))

(defun cdadr (pair)
  "return the cdr of the car of the cdr of PAIR"
  (cdr (cadr pair)))

(defun cddar (pair)
  "return the cdr of the cdr of the car of PAIR"
  (cdr (cdar pair)))

(defun cdddr (pair)
  "return the cdr of the cdr of the cdr of PAIR"
  (cdr (cddr pair)))

(defun caaaar (pair)
  "return the car of the car of the car of the car of PAIR"
  (caar (caar pair)))

(defun caaadr (pair)
  "return the car of the car of the car of the cdr of PAIR"
  (caar (cadr pair)))

(defun caadar (pair)
  "return the car of the car of the cdr of the car of PAIR"
  (caar (cdar pair)))

(defun caaddr (pair)
  "return the car of the car of the cdr of the cdr of PAIR"
  (caar (cddr pair)))

(defun cadaar (pair)
  "return the car of the cdr of the car of the car of PAIR"
  (cadr (caar pair)))

(defun cadadr (pair)
  "return the car of the cdr of the car of the cdr of PAIR"
  (cadr (cadr pair)))

(defun caddar (pair)
  "return the car of the cdr of the cdr of the car of PAIR"
  (cadr (cdar pair)))

(defun cadddr (pair)
  "return the car of the cdr of the cdr of the cdr of PAIR"
  (cadr (cddr pair)))

(defun cdaaar (pair)
  "return the cdr of the car of the car of the car of PAIR"
  (cdar (caar pair)))

(defun cdaadr (pair)
  "return the cdr of the car of the car of the cdr of PAIR"
  (cdar (cadr pair)))

(defun cdadar (pair)
  "return the cdr of the car of the cdr of the car of PAIR"
  (cdar (cdar pair)))

(defun cdaddr (pair)
  "return the cdr of the car of the cdr of the cdr of PAIR"
  (cdar (cddr pair)))

(defun cddaar (pair)
  "return the cdr of the cdr of the car of the car of PAIR"
  (cddr (caar pair)))

(defun cddadr (pair)
  "return the cdr of the cdr of the car of the cdr of PAIR"
  (cddr (cadr pair)))

(defun cdddar (pair)
  "return the cdr of the cdr of the cdr of the car of PAIR"
  (cddr (cdar pair)))

(defun cddddr (pair)
  "return the cdr of the cdr of the cdr of the cdr of PAIR"
  (cddr (cddr pair)))

(fset 'not #'null)
(fset 'string-concat #'concat)

(defun atom (ob)
  "return true if OBJECT is an atom, i.e. not a pair"
  (null (consp ob)))

(defun expt (base power)
  "raise BASE to POWER"
  (if (zerop power)
      1
    (* base (expt base (1- power)))))

(defun <= (&rest args)
  "return t if number args are in ascending order (non-strict), else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (or (< n 2nd)
               (= n 2nd))
           (apply #'<= (cdr args))))))

(defun > (&rest args)
  "return t if number args are in strictly descending order, else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (and (not (< n 2nd))
                (not (= n 2nd)))
           (apply #'> (cdr args))))))

(defun >= (n &rest args)
  "return t if number args are in descending order (non-strict), else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (not (< n 2nd))
           (apply #'>= (cdr args))))))

(defun /= (&rest args)
  "return t if all number args are different, nil else"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (not (= n 2nd))
           (apply #'/= (cons n (cddr args)))
           (apply #'/= (cdr args))))))

(defun string (&rest args)
  "make a string from all args and return it"
  (apply #'concat (map #'princs args)))

(defun string<= (&rest args)
  "return t if string args are in ascending order (non-strict), else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (or (string< n 2nd)
               (string= n 2nd))
           (apply #'string<= (cdr args))))))

(defun string> (&rest args)
  "return t if string args are in strictly descending order, else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (and (not (string< n 2nd))
                (not (string= n 2nd)))
           (apply #'string> (cdr args))))))

(defun string>= (n &rest args)
  "return t if string args are in descending order (non-strict), else nil"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (not (string< n 2nd))
           (apply #'string>= (cdr args))))))

(defun string/= (&rest args)
  "return t if all string args are different, nil else"
  (if (or (null args) (null (cdr args)))
      t
    (let ((n (car args))
          (2nd (cadr args)))
      (and (not (string= n 2nd))
           (apply #'string/= (cons n (cddr args)))
           (apply #'string/= (cdr args))))))

(defun length (list)
  "return the length of LIST"
  (if (null list)
      0
    (1+ (length (cdr list)))))

(defun list* (&rest args+)
  "list of all args, with last arg as the cdr of the last pair constructed"
  (if (null (cdr args+))
      (car args+)
    (cons (car args+)
          (apply #'list* (cdr args+)))))

(defun head (l n)
  "of list L, take the first N elements"
  (if (null l)
      n
    (if (<= n 0)
        nil
      (cons (car l) (head (cdr l) (1- n))))))

(defun skip (l n)
  "of list L, skip the first N elements"
  (if (null l)
      l
    (if (<= n 0)
        l
      (skip (cdr l) (1- n)))))

(defun tail (l n)
  "of list L, take the last N elements"
  (if (null l)
      l
    (skip l (- (length l) n))))



(defun sort (l pred)
  "sort list L with predicate PRED and return the resulting list"
  (if (or (null l) (null (cdr l)))
      l
    (let* ((len (length l))
           (half (div len 2))
           (l1 (sublist l 0 half))
           (l2 (sublist l half))
           (merge (lambda (l1 l2)
                    (if (null l1)
                        l2
                      (if (null l2)
                          l1
                        (if (pred (car l1) (car l2))
                            (cons (car l1) (merge (cdr l1) l2))
                          (cons (car l2) (merge l1 (cdr l2)))))))))
      (merge (sort l1 pred) (sort l2 pred)))))


(defun make-string (n el)
  "return a string of N occurences of element EL (may be a function)"
  (if (zerop n)
      ""
    (concat (if (functionp el)
                (el)
              (string el))
            (make-string (1- n) el))))

(defun join (sep &rest args)
  "append all ARGS (or their elements) to a string, separated by SEP"
  (if (null args)
      ""
    (let* ((left (if (not (listp (car args)))
                     (princs (car args))
                   (apply #'join sep (car args))))
           (right (apply #'join sep (cdr args))))
      (if (string= "" left)
          right
        (if (string= "" right)
            left
          (concat left sep right))))))

(defun 1+ (n) (+ n 1))
(defun 1- (n) (- n 1))
(defun 2+ (n) (+ n 2))
(defun 2- (n) (- n 2))

(defun zerop (n)
  (= n 0))

(defun nth (n l)
  (if (zerop n)
      (car l)
    (nth (1- n) l)))

(defun elt (l n)
  "of list L, return element N (zero-based), or throw an error"
  (if (null l)
      (error "elt: index out of range")
    (if (zerop n)
        (car l)
      (elt (cdr l)
           (1- n)))))

(defun last (l)
  "Return the last element of list L."
  (car (last-pair l)))

(defun last-pair (l)
  "return the last pair of list L"
  (if (null (cdr l))
      l
    (last-pair (cdr l))))

(defun filter (predicate l)
  "Return a list of elements from list L for which PREDICATE is true."
  (if l
      (if (predicate (car l))
          (cons (car l) (filter predicate (cdr l)))
        (filter predicate (cdr l)))
    nil))

(defun all (&rest args)
  "return true if all arguments are true"
  (if (null args)
      t
    (if (car args)
        (apply #'all (cdr args))
      nil)))

(defun map (func list)
  "apply FUNC to each element of LIST and return the list of results"
  (if (null list)
      nil
    (cons (func (car list))
          (map func (cdr list)))))

(defun mapcar (fun &rest lists)
  "apply FUN to all elements of all LISTS and return a list of the results"
  (labels ((mapcar1 (fun arglist)
                    (if (null arglist)
                        nil
                      (cons (funcall fun (car arglist))
                            (mapcar1 fun (cdr arglist))))))
    (if (not (apply #'all lists))       ;any of them null?
        nil
      (let ((args (mapcar1 #'car lists))
            (next (mapcar1 #'cdr lists)))
        (cons (apply fun args)
              (apply #'mapcar (cons fun next)))))))


(defun progn (&rest body)
  (last body))

(defun prog1 (&rest body)
  (car body))

(defun prog2 (&rest body)
  (cadr body))

(defun nconc (&rest lists)
  "concatenate all lists by modifying their last cons and return the result"
  (while (and lists (null (car lists)))
    (pop lists))
  (if (null lists)
      nil
    (let* ((result (car lists))
           (l result))
      (while (cdr l)
        (setq l (cdr l)))
      (rplacd l (apply #'nconc (cdr lists)))
      result)))      

(defun append (l1 l2)
  (if (null l1)
      l2
    (cons (car l1)
          (append (cdr l1) l2)))) 

(defun reverse (l)
  (if (null l)
      l
    (let ((first (car l)))
      (append (reverse (cdr l))
              (list first)))))

(defun nreverse (l)
  (let ((new))
    (while (consp l)
      (let ((this l))
        (setq l (cdr l))
        (rplacd this new)
        (setq new this)))
    new))

(defun equal (ob1 ob2)
  (or (eq ob1 ob2)
      (if (consp ob1)
          (and (consp ob2)
               (equal (car ob1) (car ob2))
               (equal (cdr ob1) (cdr ob2))))))

(defun make-list (n el)
  "Return a list of length N with elements EL (which may be a function)."
  (let (result)
    (if (functionp el)
        (while (> n 0)
          (push (funcall el) result)
          (decf n))
      (while (> n 0)
        (push el result)
        (decf n)))
    (nreverse result)))

(defun identity (ob)
  "return the argument"
  ob)

(defun ignore (&rest args)
  "ignore all arguments and return nil"
  nil)

(defun list (&rest args)
  "return a new list from the arguments"
  args)

(defun assoc (item alist)
  "return the pair of ALIST whose car is equal to ITEM, or nil"
  (if (null alist)
      nil
    (if (equal item (caar alist))
        (car alist)
      (assoc item (cdr alist)))))

(defun assq (item alist)
  "return the pair of ALIST whose car is eq to ITEM, or nil"
  (if (null alist)
      nil
    (if (eq item (caar alist))
        (car alist)
      (assq item (cdr alist)))))

(defun sassoc (item alist func)
    "return the pair of ALIST whose car is equal to ITEM, or call FUNC"
  (if (null alist)
      (func)
    (if (equal item (caar alist))
        (car alist)
      (sassoc item (cdr alist) func))))

(defun sassq (item alist func)
    "return the pair of ALIST whose car is equal to ITEM, or call FUNC"
  (if (null alist)
      (func)
    (if (eq item (caar alist))
        (car alist)
      (sassq item (cdr alist) func))))

(defun remove (item l &optional test)
  "delete the occurrences of ITEM from list L and return the result"
  (setq test (or test #'eq))
  (if (null l)
      nil
    (if (test item (car l))
        (remove item (cdr l) test)
      (cons (car l) (remove item (cdr l) test)))))

(defun delete (item l &optional test)
  "delete the occurrences of ITEM from list L and return the result, may modify"
  ;; but does not, currently
  (remove item l test))

