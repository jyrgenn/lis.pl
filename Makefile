GITHUB = git@github.com:jyrgenn/lis.pl
PERLSOURCES = $(shell ls *.pm *.pl)
CHECKOPTS = # -MCarp::Always

all: check uses.dot test

check:
	for src in $(PERLSOURCES); do perl $(CHECKOPTS) -c $$src; done

uses.dot: $(PERLSOURCES) Makefile
	(echo digraph {; \
	 for file in $(PERLSOURCES); do \
	     sed -n 's/^use  *\(.*\) *;.*/\1/p' $$file | while read use; do \
	         if [ -f "$$use.pm" ]; then \
	             echo "   \"$$file\" -> \"$$use.pm\""; \
	         fi; \
	     done; \
	 done; \
	 echo }) > uses.dot

test:
	./Lis.pl tests/run-tests.lisp

tags: $(PERLSOURCES) *.lisp
	etags $(PERLSOURCES)

profile:
	perl -d:NYTProf Lis.pl tests/run-tests.lisp
	nytprofhtml nytprof.out
	echo http://64927.de/nytprof/

step-tests:
	for i in tests/0*.lisp; do \
		./Lis.pl tests/run-tests.lisp $$i; \
		echo $$i; \
		read; \
	done

push:
	time make test > /dev/null
	git push
	git push $(GITHUB)

fast:
	./Lis.pl -l l/factor.lisp -e '(factor 10000001)'

clean:
	rm -f *~ */*~ \#*# TAGS
