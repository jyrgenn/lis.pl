-*- text -*-

A Lisp in Perl
==============

Because we can! Writing a Lisp interpreter not in a language I am
currently learning, but relatively familiar with shifts the "how do
I do this" thinking from "... at all" to "... in the most
convenient/elegant/best of all ways". That doesn't mean it is
easier. Still, I get to learn new things about Perl all the time.

The Lisp dialect implemented here is closely related to that of the
"lingo" implementation -- mostly traditional, names from Maclisp or
Common Lisp, mandatory defvar for global variables. It is different
in having fewer datatypes (vectors, tables, ports) and no file I/O,
but mainly by using lexical scope for bindings, which has the great
advantage of enabling closures. (I *love* closures!)

The implementation is quite simple. In particular it is a very naive
interpreter that does the same things over and over, not a hint of
optimization or the like, just like "lingo". (It does about 35000
evals/s on my laptop, which is not exactly fast.) Still it is fun to
play with it and try things out.

It is probably possible to make this implementation more idiomatic
in terms of Perl idioms. It shows that I am not really a "Perl
programmer" who knows the ins and outs of Perl and how to apply them
best, but a programmer in general with some working knowledge of
Perl who could have done this in some other language just as well.
Oh, wait, I did...
